from src.cli import get_command_line_arguments
from src.input import parse_therapy_synthesis_config_file
from src.therapy_synthesis import synthesize_therapy
from src.utils import dump_synthesis_results


def run():
    print("= Therapy synthesis optimization settings =")
    cli_args = get_command_line_arguments()

    model, virtual_patient, initial_treatment, \
        exclude_from_initialization, \
        horizon, sim_engine, solver, \
        solver_parameters, output_filename = parse_therapy_synthesis_config_file(cli_args.yaml, cli_args)

    optimized_therapy, objective_value, \
        function_evaluations, statistics = synthesize_therapy(model, virtual_patient, initial_treatment,
                                                              exclude_from_initialization, horizon, sim_engine,
                                                              solver, solver_parameters)

    dump_synthesis_results(model, sim_engine, horizon, optimized_therapy, objective_value,
                           function_evaluations, statistics, output_filename)


if __name__ == '__main__':
    print("=========== Synthesis of Personalized Therapy with COPASI ===========\n"
          "=========== For reviewers ===========")
    run()

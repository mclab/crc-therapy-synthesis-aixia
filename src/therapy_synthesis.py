from typing import Dict, List

import codetiming as ct
from apricopt.model.Model import Model
from apricopt.simulation.SimulationEngine import SimulationEngine
from apricopt.solving.whitebox.WhiteBoxSolver import WhiteBoxSolver
from apricopt.solving.whitebox.COPASI.COPASISolverParameter import COPASISolverParameter


def synthesize_therapy(model: Model, virtual_patient: Dict[str, float],
                       initial_treatment: Dict[str, float],
                       exclude_from_initialization: List[str],
                       time_horizon: float, sim_engine: SimulationEngine,
                       solver: WhiteBoxSolver,
                       solver_parameters: List[COPASISolverParameter]) -> (Dict[str, float], float, int, dict):
    """
    :param model:
    :param virtual_patient:
    :param initial_treatment:
    :param exclude_from_initialization:
    :param time_horizon:
    :param sim_engine:
    :param solver:
    :param solver_parameters:
    :return:
    """
    print("\n\n============ Therapy Synthesis through COPASI Solver ===========")

    timer = ct.Timer()
    timer.logger = None

    timer.start()
    print("Virtual patient initialisation..")
    init_time = virtual_patient['init_time']
    vp = {param_name: virtual_patient[param_name] for param_name in virtual_patient if
          param_name not in ['id', 'admissible', 'init_time', 'conditionId']}
    model.set_fixed_params(vp)
    sim_engine.simulate_and_set(model, init_time, exclude=exclude_from_initialization,
                                evaluate_constraints=False)
    print(f"Virtual patient intialisation time: {timer.stop():.2f} sec")

    model.set_parameters_nominal_values(initial_treatment)

    simulation_model = model.copy()
    simulation_model.set_fixed_params(initial_treatment)
    trajectory = sim_engine.simulate_trajectory(simulation_model, time_horizon)
    print(f"Initial treatment total drugs: {trajectory['total_drugs'][-1]:.2f}%")

    print("Start therapy optimisation...")
    timer.start()
    optimized_therapy, objective_value, \
     function_evaluations, statistics = solver.solve(model, time_horizon, sim_engine, solver_parameters)
    print("Therapy optimisation finished..")
    print(f"Therapy optimisation elapsed time: {timer.stop():.2f} sec")

    print(f"\n\nBest value found in {function_evaluations} function evaluations: \n\n{optimized_therapy}\n\n")

    return optimized_therapy, objective_value, function_evaluations, statistics


from apricopt.solving.whitebox.COPASI.COPASIOptimisationMethod import COPASIOptimisationMethod
from apricopt.solving.whitebox.COPASI.COPASISolverParameter import COPASISolverParameter
from apricopt.solving.whitebox.COPASI.method.COPASIGeneticAlgorithm import COPASIGeneticAlgorithm
from apricopt.solving.whitebox.COPASI.method.COPASIParticleSwarm import COPASIParticleSwarm
from apricopt.solving.whitebox.COPASI.method.COPASINelderMead import COPASINelderMead
from apricopt.solving.whitebox.COPASI.method.COPASIRandomSearch import COPASIRandomSearch
from apricopt.solving.whitebox.COPASI.method.COPASITruncatedNewton import COPASITruncatedNewton
from apricopt.solving.whitebox.COPASI.method.COPASIEvolutionaryStrategySR import COPASIEvolutionaryStrategySR
from apricopt.solving.whitebox.COPASI.method.COPASIPraxis import COPASIPraxis
from apricopt.solving.whitebox.COPASI.method.COPASIHookeJeeves import COPASIHookeJeeves
from apricopt.solving.whitebox.COPASI.method.COPASIEvolutionaryProgramming import COPASIEvolutionaryProgramming
from apricopt.solving.whitebox.COPASI.method.COPASISteepestDescent import COPASISteepestDescent
from apricopt.solving.whitebox.COPASI.method.COPASIDifferentialEvolution import COPASIDifferentialEvolution
from apricopt.solving.whitebox.COPASI.method.COPASIScatterSearch import COPASIScatterSearch
from apricopt.model.Model import Model
from apricopt.simulation.SimulationEngine import SimulationEngine

from typing import Dict, Union, List
import json


def get_optimisation_method(optimisation_method_string: str) -> COPASIOptimisationMethod:
    if optimisation_method_string.lower() in ['genetic algorithm', 'geneticalgorithm', 'ga']:
        print("Optimisation method: Genetic Algorithm")
        return COPASIGeneticAlgorithm()
    elif optimisation_method_string.lower() in ['particle swarm', 'particleswarm', 'pm']:
        print("Optimisation method: Particle Swarm")
        return COPASIParticleSwarm()
    elif optimisation_method_string.lower() in ['nelder-mead', 'nelder mead', 'neldermead', 'nm']:
        print("Optimisation method: Nelder Mead")
        return COPASINelderMead()
    elif optimisation_method_string.lower() in ['random search', 'random-search', 'randomsearch', 'rs']:
        print("Optimisation method: Random Search")
        return COPASIRandomSearch()
    elif optimisation_method_string.lower() in ['evolutionary strategy sr', 'evolutionary-strategy-sr',
                                                'evolutionarystrategysr', 'sres']:
        print("Optimisation method: Evolutionary Strategy SR")
        return COPASIEvolutionaryStrategySR()
    elif optimisation_method_string.lower() in ['praxis']:
        print("Optimisation method: Praxis")
        return COPASIPraxis()
    elif optimisation_method_string.lower() in ['hookejeeves', 'hj']:
        print("Optimisation method: Hooke & Jeeves")
        return COPASIHookeJeeves()
    elif optimisation_method_string.lower() in ['evolutionary programming', 'evolutionary-programming',
                                                'evolutionaryprogramming', 'ep']:
        print("Optimisation method: EvolutionaryProgramming")
        return COPASIEvolutionaryProgramming()
    elif optimisation_method_string.lower() in ['steepest descent', 'steepest-descent',
                                                'steepestdescent', 'sd']:
        print("Optimisation method: Steepest Descent")
        return COPASISteepestDescent()
    elif optimisation_method_string.lower() in ['differential evolution', 'differentialevolution',
                                                'differential-evolution', 'de']:
        print("Optimisation method: Differential Evolution")
        return COPASIDifferentialEvolution()
    elif optimisation_method_string.lower() in ['scatter search', 'scattersearch', 'scatter-search',
                                              'sc']:
        print("Optimisation method: Scatter Search")
        return COPASIScatterSearch()
    else:
        raise ValueError(f"The optimisation method {optimisation_method_string} is not supported.")


def get_solver_parameters(optimisation_method: COPASIOptimisationMethod,
                          input_method_parameters: Dict[str, Union[int, float]]) -> List[COPASISolverParameter]:

    method_parameters = []
    if isinstance(optimisation_method, COPASIGeneticAlgorithm):
        method_parameters = [COPASIGeneticAlgorithm.SEED,
                             COPASIGeneticAlgorithm.POPULATION_SIZE,
                             COPASIGeneticAlgorithm.NUMBER_OF_GENERATIONS,
                             COPASIGeneticAlgorithm.MUTATION_VARIANCE,
                             COPASIGeneticAlgorithm.RANDOM_NUMBER_GENERATOR]

    elif isinstance(optimisation_method, COPASIParticleSwarm):
        method_parameters = [COPASIParticleSwarm.SWARM_SIZE,
                             COPASIParticleSwarm.STD_DEVIATION,
                             COPASIParticleSwarm.ITERATION_LIMIT,
                             COPASIParticleSwarm.SEED,
                             COPASIParticleSwarm.RANDOM_NUMBER_GENERATOR]

    elif isinstance(optimisation_method, COPASINelderMead):
        method_parameters = [COPASINelderMead.ITERATION_LIMIT,
                             COPASINelderMead.TOLERANCE,
                             COPASINelderMead.SCALE]

    elif isinstance(optimisation_method, COPASIRandomSearch):
        method_parameters = [COPASIRandomSearch.NUMBER_OF_ITERATIONS,
                             COPASIRandomSearch.RANDOM_NUMBER_GENERATOR,
                             COPASIRandomSearch.SEED]

    elif isinstance(optimisation_method, COPASIEvolutionaryStrategySR):
        method_parameters = [COPASIEvolutionaryStrategySR.SEED,
                             COPASIEvolutionaryStrategySR.POPULATION_SIZE,
                             COPASIEvolutionaryStrategySR.NUMBER_OF_GENERATIONS,
                             COPASIEvolutionaryStrategySR.RANDOM_NUMBER_GENERATOR,
                             COPASIEvolutionaryStrategySR.PF]

    elif isinstance(optimisation_method, COPASIPraxis):
        method_parameters = [COPASIPraxis.TOLERANCE]

    elif isinstance(optimisation_method, COPASIHookeJeeves):
        method_parameters = [COPASIHookeJeeves.TOLERANCE,
                             COPASIHookeJeeves.ITERATION_LIMIT,
                             COPASIHookeJeeves.RHO]

    elif isinstance(optimisation_method, COPASIEvolutionaryProgramming):
        method_parameters = [COPASIEvolutionaryProgramming.SEED,
                             COPASIEvolutionaryProgramming.POPULATION_SIZE,
                             COPASIEvolutionaryProgramming.NUMBER_OF_GENERATIONS,
                             COPASIEvolutionaryProgramming.RANDOM_NUMBER_GENERATOR]

    elif isinstance(optimisation_method, COPASISteepestDescent):
        method_parameters = [COPASISteepestDescent.ITERATION_LIMIT,
                             COPASISteepestDescent.TOLERANCE]

    elif isinstance(optimisation_method, COPASIDifferentialEvolution):
        method_parameters = [COPASIDifferentialEvolution.SEED,
                             COPASIDifferentialEvolution.POPULATION_SIZE,
                             COPASIDifferentialEvolution.NUMBER_OF_GENERATIONS,
                             COPASIDifferentialEvolution.MUTATION_VARIANCE,
                             COPASIDifferentialEvolution.RANDOM_NUMBER_GENERATOR]

    elif isinstance(optimisation_method, COPASIScatterSearch):
        method_parameters = [COPASIScatterSearch.SEED,
                             COPASIScatterSearch.NUMBER_OF_ITERATIONS,
                             COPASIScatterSearch.RANDOM_NUMBER_GENERATOR]

    copasi_parameter_objects = []
    for param in method_parameters:
        if param in input_method_parameters.keys():
            copasi_parameter_objects.append(
                COPASISolverParameter(param, input_method_parameters[param])
            )

    return copasi_parameter_objects


def dump_synthesis_results(model: Model, sim_engine: SimulationEngine, horizon: float,
                           optimized_therapy: Dict[str, float], objective_value: float,
                           function_evaluations: int, statistics: dict, output_filename: str) -> None:

    model.set_fixed_params(optimized_therapy)
    observations = sim_engine.simulate_trajectory(model, horizon)

    data = dict()
    for obs_id, obs_val in observations.items():
        if obs_id != 'time':
            data[obs_id] = obs_val[-1]

    data['synthesized_therapy'] = optimized_therapy
    data['objective_value'] = objective_value
    data['function_evaluations'] = function_evaluations
    data['statistics'] = statistics

    with open(output_filename, 'w') as f:
        json.dump(data, f, indent=4)

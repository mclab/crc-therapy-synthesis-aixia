python /mnt/nfs/lpicchiami/therapy-synthesis-rcra/run.py --config=/mnt/nfs/lpicchiami/therapy-synthesis-rcra/case_studies/CRC/therapy_synthesis/input/yaml/experiments/genetic_algorithm/therapy_synthesis_genetic_algorithm_11_patients_23_parameters.yaml -l /mnt/exps/lpicchiami/rcra-2021-treatments/patients_23_parameters/genetic_algorithm/virtual_patient_8/log_virtual_patient_8_genetic_algorithm_synthesis.txt -p 8 -o /mnt/exps/lpicchiami/rcra-2021-treatments/patients_23_parameters/genetic_algorithm/virtual_patient_8/virtual_patient_8_genetic_algorithm.json


--------------------------------------------------------------------------------


=========== Therapy synthesis optimisation settings ========
Optimisation method: Genetic Algorithm
The user-defined hyperparameters values are:
	Seed: 104
	Population Size: 64
	Number of Generations: 25


============ Therapy Synthesis through COPASI Solver ===========
Virtual patient initialisation..
Virtual patient intialisation time: 4.93 sec
Initial treatment total drugs: 100.00%
Start therapy optimisation...
Therapy optimisation finished..
Therapy optimisation elapsed time: 2951.28 sec


Best value found in 1634 function evaluations: 

{'cibi_m3_in': 1.57271645810088e-258, 'cibi_m10_in': 1.3123818861510385e-256, 'ate_m11_in': 3.935287891868329e-219, 'ate_m1_in': 9.598137593629279, 'ate_freq_in': 5.643050406621649, 'cibi_m2_in': 9.75096366571436e-310, 'cibi_m12_in': 2.4961628161693246e-166, 'cibi_m6_in': 3.1105225261041793e-107, 'cibi_m13_in': 2.7394312545061646e-233, 'ate_m8_in': 2.512152613445625e-40, 'ate_m15_in': 6.383386357370274e-45, 'ate_m5_in': 6.759181253044409e-236, 'ate_m3_in': 9.243191263971402e-140, 'ate_m12_in': 7.814690576122845e-112, 'cibi_m14_in': 1.4291981989972194e-100, 'ate_m9_in': 1.1543332923150387e-75, 'ate_m2_in': 1.8651527230432543e-93, 'ate_m4_in': 2.17603670161509e-223, 'ate_m13_in': 1.166775656780561e-275, 'cibi_m15_in': 0.0, 'cibi_m9_in': 9.046238588640184e-16, 'ate_m6_in': 1.0703287054364695e-117, 'cibi_m5_in': 0.0, 'cibi_freq_in': 4.522396709637524, 'cibi_m4_in': 3.7904916318555156e-50, 'ate_m14_in': 3.712852586145757e-44, 'cibi_m11_in': 1.90892135642837e-18, 'ate_m10_in': 6.660566552052608e-122, 'cibi_m7_in': 0.0, 'cibi_m1_in': 89.65380677059954, 'cibi_m8_in': 0.0, 'ate_m7_in': 3.4683643763069447e-60}



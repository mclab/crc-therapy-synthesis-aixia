=========== Therapy synthesis optimisation settings ========
scatter search
Optimisation method: Scatter Search
The user-defined hyperparameters values are:
	Seed: 104
	Number of Iterations: 500


============ Therapy Synthesis through COPASI Solver ===========
Virtual patient initialisation..
Virtual patient intialisation time: 1.01 sec
Initial treatment total drugs: 100.00%
Start therapy optimisation...

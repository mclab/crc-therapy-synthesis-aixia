======= APRICOPT =========

=== Optimization through COPASI solver ===


Solver settings: 

Method: Evolution Strategy (SRES)
    Log Verbosity: 0
    Number of Generations: 25
    Population Size: 64
    Random Number Generator: 1
    Seed: 104
    Pf: 0.1
    Stop after # Stalled Generations: 0

Population Information: 
Population Size: 0
# Generations / Iterations: 0
Current Generation / Iteration: 0
Population Values: 
   (		)

Population:



Optimization Log:

Function Evaluation	Objective Value		Optimization Parameters
64			37.9491			(	5.40083	8.74303	0.807403	19.9345	10.9096	4.96087	14.8229	10.8567	18.3186	7.5391	5.07546	11.7877	13.8214	1.92371	12.3442	7.29644	5.15068	135.754	118.754	119.151	84.4024	12.1764	26.7676	116.167	17.8439	62.6581	63.5973	61.5388	157.248	66.3212	63.0366	6.0081	)
448			37.6418			(	5.43975	4.41471	1.87779	19.2751	10.6448	5.15398	14.4664	10.1655	17.6099	7.91358	4.62596	8.41573	15.5128	2.06342	9.56543	6.5925	5.40869	123.129	103.114	114.879	86.9251	4.10028	29.716	95.6304	21.0328	61.006	61.1478	42.6262	142	54.9197	59.2858	2.89953	)

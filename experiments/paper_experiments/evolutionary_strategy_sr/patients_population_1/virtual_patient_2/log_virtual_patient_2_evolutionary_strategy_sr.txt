=========== Therapy synthesis optimisation settings ========
Optimisation method: Evolutionary Strategy SR
The user-defined hyperparameters values are:
	Seed: 104
	Population Size: 64
	Number of Generations: 25
	Pf: 0.1


============ Therapy Synthesis through COPASI Solver ===========
Virtual patient initialisation..
Virtual patient intialisation time: 1.09 sec
Initial treatment total drugs: 100.00%
Start therapy optimisation...

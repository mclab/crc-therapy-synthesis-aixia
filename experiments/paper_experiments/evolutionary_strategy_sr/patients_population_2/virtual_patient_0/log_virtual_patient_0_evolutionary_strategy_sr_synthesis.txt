======= APRICOPT =========

=== Optimization through COPASI solver ===


Solver settings: 

Method: Evolution Strategy (SRES)
    Log Verbosity: 0
    Number of Generations: 25
    Population Size: 64
    Random Number Generator: 1
    Seed: 104
    Pf: 0.1
    Stop after # Stalled Generations: 0

Population Information: 
Population Size: 0
# Generations / Iterations: 0
Current Generation / Iteration: 0
Population Values: 
   (		)

Population:



Optimization Log:

Function Evaluation	Objective Value		Optimization Parameters
64			28.3644			(	4.65297	18.0879	1.53942	14.9822	18.2728	7.126	17.6478	9.23296	2.25	2.23498	18.9518	12.5628	8.78745	9.13054	9.96132	14.3692	5.80167	35.0134	21.1735	22.3924	54.1718	5.79287	0.498024	48.2121	108.966	2.63332	4.78962	75.4107	130.633	105.416	119.986	88.2872	)
448			28.1046			(	5.43975	4.41471	1.87779	19.2751	10.6448	5.15398	14.4664	10.1655	17.6099	7.91358	4.62596	8.41573	15.5128	2.06342	9.56543	6.5925	5.40869	123.129	103.114	114.879	86.9251	4.10028	29.716	95.6304	21.0328	61.006	61.1478	42.6262	142	54.9197	59.2858	2.89953	)

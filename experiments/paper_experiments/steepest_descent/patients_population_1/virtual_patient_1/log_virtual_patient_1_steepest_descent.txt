=========== Therapy synthesis optimisation settings ========
steepest descent
Optimisation method: Steepest Descent
The user-defined hyperparameters values are:
	Iteration Limit: 300
	Tolerance: 1e-5


============ Therapy Synthesis through COPASI Solver ===========
Virtual patient initialisation..
Virtual patient intialisation time: 0.87 sec
Initial treatment total drugs: 100.00%
Start therapy optimisation...

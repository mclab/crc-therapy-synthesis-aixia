======= APRICOPT =========

=== Optimization through COPASI solver ===


Solver settings: 

Method: Differential Evolution
    Log Verbosity: 0
    Number of Generations: 25
    Population Size: 64
    Random Number Generator: 1
    Seed: 104
    Mutation Variance: 0.1
    Stop after # Stalled Generations: 0

Population Information: 
Population Size: 0
# Generations / Iterations: 0
Current Generation / Iteration: 0
Population Values: 
   (		)

Population:



Optimization Log:

Function Evaluation	Objective Value		Optimization Parameters
1			19.4748			(	4	24	24	24	24	24	24	24	24	24	24	24	24	24	24	24	3	160	160	160	160	160	160	160	160	160	160	160	160	160	160	160	)
199			16.1109			(	4.80663	22.9405	4.80397e-305	19.4793	24	24	7.40926e-109	23.2766	1.72308e-134	24	23.3277	7.54471e-252	24	22.5662	21.838	1.52914e-258	3.43125	160	160	160	153.332	160	9.61864e-157	160	160	0.158386	3.78452e-112	3.56745e-110	1.49614e-90	156.36	1.17571e-50	160	)
334			14.928			(	3.90841	24	0	24	24	24	1.09509e-40	24	6.40807e-31	24	24	0	24	24	24	1.92793e-63	6	160	160	160	160	160	0	160	160	0.316772	2.7611e-100	7.13489e-110	1.82834e-44	160	2.35142e-50	160	)
1009			13.4073			(	5.63707	1.50906e-113	23.6746	0	1.20411e-67	8.9319e-42	24	23.6758	20.1476	20.886	0	22.8706	22.9577	1.2595e-166	24	24	5.14063	0.0383373	0	2.52297e-166	1.58532e-54	3.48369e-43	153.177	2.41629e-135	140.405	8.1893e-93	1.40158e-157	160	0	3.40775e-34	152.859	2.20356e-22	)

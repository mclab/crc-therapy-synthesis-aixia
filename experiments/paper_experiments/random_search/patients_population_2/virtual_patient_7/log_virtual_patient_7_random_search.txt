=========== Therapy synthesis optimisation settings ========
Optimisation method: Random Search
The user-defined hyperparameters values are:
	Number of Iterations: 600
	Seed: 104


============ Therapy Synthesis through COPASI Solver ===========
Virtual patient initialisation..
Virtual patient intialisation time: 2.42 sec
Initial treatment total drugs: 100.00%
Start therapy optimisation...

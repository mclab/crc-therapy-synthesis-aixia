#!/bin/bash

echo "Generation of the ranking plot through (stacked) objective value..."
python code/all_algorithms_stacked_objective.py --all-data data/all_patients_optimisation_data.json  \
       --output figures/algorithms/all-algo-objective.svg

echo "Generation of the ranking plot through KPIs..."
python code/all_algorithms_kpis.py --all-data data/all_patients_optimisation_data.json  \
       --reference data/reference_treatment_all_patients.csv \
       --outplot1 figures/algorithms/drug-saving-square.svg \
       --outplot2 figures/algorithms/all-algo-tumour-size-gain.svg

echo "Generation of the ranking plot of best winner algorithms (in terms of VPs)..."
python code/ranking_algorithms.py --analysis-data data/analysis_results_by_KPI.json\
       --output  figures/ranking/ranking-algorithms.svg

echo "Generation of the RECIST plot w.r.t. best 5 optimisation algorithms..."
python code/recist_best_algorithms.py --optimisation-data data/all_patients_optimisation_data.json \
       --reference-data data/all_patients_reference_data.json \
       --output  figures/recist/recist-best-algorithms.svg\
       --population1 ../../case_studies/CRC/virtual_population/11_patients_population_1.tsv \
       --population2 ../../case_studies/CRC/virtual_population/14_patients_population_2.tsv

echo "Genetion of the time-to-response plot..."
python code/time_to_response.py --kpi-data data/analysis_results_by_KPI.json \
       --output  figures/time_to_response/time-to-response-algorithms.svg

echo "Generation of the tumour over time plot (untreated patients)..."
python code/tumour_over_time.py --no-treatment data/no_treatment_tumour_all_patients.csv \
       --output  figures/treatment_over_time/tumour-untreated.svg
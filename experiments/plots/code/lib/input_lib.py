import pandas as pd
import argparse as arg


def get_frame_data(csv_filename: str) -> pd.DataFrame:
    frame = pd.read_csv(csv_filename)
    return frame

def get_command_line_arguments_stacked_objective():
    parser = arg.ArgumentParser(description="Process optimisation input for all algorithms plot")

    parser.add_argument('--all-data', type=str, dest='all_data',
                        help="The path for the JSON of all optimisation experiments",
                        required=True)

    parser.add_argument('-o', '--output', type=str, dest='output',
                        help="Plot output path.",
                        required=True)

    arguments = parser.parse_args()

    return arguments.all_data, arguments.output

def get_command_line_arguments_drug_inefficacy():
    parser = arg.ArgumentParser(description="Process optimisation input for all algorithms plot")

    parser.add_argument('--all-data', type=str, dest='all_data',
                        help="The path for the JSON of all optimisation experiments",
                        required=True)

    parser.add_argument('--reference', type=str, dest='reference',
                        help="The path for the CSV of Reference Treatment experiments",
                        required=True)

    parser.add_argument('-o1', '--outplot1', type=str, dest='out_plot1',
                        help="Plot 1 output path.",
                        required=True)

    parser.add_argument('-o2', '--outplot2', type=str, dest='out_plot2',
                        help="Plot 2 output path.",
                        required=True)

    arguments = parser.parse_args()

    return arguments.all_data, arguments.reference, arguments.out_plot1, arguments.out_plot2


def get_command_line_arguments_time_to_response():
    parser = arg.ArgumentParser(description="Process optimisation input for all algorithms plot")

    parser.add_argument('--kpi-data', type=str, dest='kpi_data',
                        help="The path for the JSON of all optimisation experiments",
                        required=True)

    parser.add_argument('-o', '--output', type=str, dest='out_plot',
                        help="Plot 1 output path.",
                        required=True)

    arguments = parser.parse_args()

    return arguments.kpi_data, arguments.out_plot


def get_command_line_arguments_ranking_algorithm():
    parser = arg.ArgumentParser(description="Process result analysis input for algorithms ranking")

    parser.add_argument('--analysis-data', type=str, dest='analysis_data',
                        help="The path for the JSON of all optimisation experiments",
                        required=True)

    parser.add_argument('-o', '--output', type=str, dest='output',
                        help="Plot output path.",
                        required=True)

    arguments = parser.parse_args()

    return arguments.analysis_data, arguments.output


def get_command_line_arguments_recist():
    parser = arg.ArgumentParser(description="Parser for RECIST script")

    parser.add_argument('--optimisation-data', type=str, dest='optimisation_data',
                        help="The path of simulation data (JSON) from optimisation algorithms",
                        required=True)

    parser.add_argument('--reference-data', type=str, dest='reference_data',
                        help="The path of simulation data (JSON) from reference therapy",
                        required=True)

    parser.add_argument('--population1', type=str, dest='population_1',
                        help="The path of population 1 (TSV)",
                        required=True)

    parser.add_argument('--population2', type=str, dest='population_2',
                        help="The path of population 2 (TSV)",
                        required=True)

    parser.add_argument('-o', '--output', type=str, dest='output',
                        help="Plot output pathname.",
                        required=True)

    arguments = parser.parse_args()

    return arguments.optimisation_data, arguments.reference_data, \
                arguments.output, arguments.population_1, arguments.population_2


def get_command_line_arguments_tumour_behaviours():
    parser = arg.ArgumentParser(description="Process optimisation input for tumour behaviours")

    parser.add_argument('--no-treatment', type=str, dest='no_treatment',
                        help="The path to the CSV of no treatment simulations",
                        required=True)

    parser.add_argument('-o', '--output', type=str, dest='output',
                        help="Plot output folder.",
                        required=True)

    arguments = parser.parse_args()

    return arguments.no_treatment, arguments.output

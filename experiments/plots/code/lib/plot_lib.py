import plotly.graph_objects as go
import plotly.subplots as tls

from typing import List


def plot_multiline(height: int, width: int, n_lines: int, names: List[str], y_data, x_data,
                   x_title: str ="Time (Day)", y_title: str ="fmol/\u03BCl") -> go.Figure:

    layout = go.Layout(plot_bgcolor='rgba(0,0,0,0)', height=height, width=width,
                       title={
                           'y': 0.9,
                           'x': 0.5,
                           'xanchor': 'center',
                           'yanchor': 'top'}, legend=dict(x=0.05, y=1.0, font=dict(size = 25)))

    fig = go.Figure(layout=layout)
    for i in range(n_lines):
        fig.add_trace(go.Scatter(x=x_data, y=y_data[i],
                             mode='lines', name=names[i]))

    fig.update_xaxes(title=x_title, linewidth=1, linecolor='black', showline=True,
                     title_font = {"size": 25}, tickfont=dict(size=22))

    fig.update_yaxes(title=y_title, linewidth=1, linecolor='black', showline=True,
                     title_font = {"size": 25}, tickfont=dict(size=22))
    return fig


def plot_line(height: int, width: int, y_data, x_data, x_title: str ="Time (Day)",
              y_title: str ="fmol/\u03BCl") -> go.Figure:

    layout = go.Layout(plot_bgcolor='rgba(0,0,0,0)', height=height, width=width,
                       title={
                           'y': 0.9,
                           'x': 0.5,
                           'xanchor': 'center',
                           'yanchor': 'top'})

    fig = go.Figure(layout=layout)
    fig.add_trace(go.Scatter(x=x_data, y=y_data,
                             mode='lines', name='lines'))

    fig.update_xaxes(title=x_title, linewidth=1, linecolor='black', showline=True,
                     title_font = {"size": 25}, tickfont=dict(size=22))
    fig.update_yaxes(title=y_title, linewidth=1, linecolor='black', showline=True,
                     title_font = {"size": 25}, tickfont=dict(size=22))
    return fig


def objective_stacked_bar(height: int, width: int, x_data: list, y_data: List[list], title: str,
                          legend: List[str],
                          y_axis_title: str, bar_color: List[str], percentage, y_error_data=None) -> go.Figure:
    if percentage:
        fig = go.Figure(layout={
            'yaxis': {'title': y_axis_title, 'ticksuffix': '%'}
        })
    else:
        fig = go.Figure(layout={
            'yaxis': {'title': y_axis_title}
        })

    fig.add_trace(go.Bar(name=legend[0], x=x_data, y=y_data[0], text=y_data[0], marker={'color': bar_color[0]}))
    fig.add_trace(go.Bar(name=legend[1], x=x_data, y=y_data[1], text=y_data[1], marker={'color': bar_color[1]}))
    fig.update_layout(height=height, width=width, title={
        'text': title,
        'y': 0.95,
        'x': 0.45,
        'xanchor': 'center',
        'yanchor': 'top'}, plot_bgcolor='rgba(0,0,0,0)', barmode='stack', legend=dict(x=0.8, y=1.1, font=dict(size=40)))

    if percentage:
        fig.update_traces(texttemplate='%{text:.2f}%', textposition='outside', textfont=dict(size=22))
    else:
        fig.update_traces(texttemplate='%{text:.2f}', textposition='outside', textfont=dict(size=31))


    fig.update_xaxes(linewidth=1, linecolor='black', showline=True,
                     showgrid=True, gridcolor='black', gridwidth=0.1,
                     title_font={"size": 32}, tickfont=dict(size=28))

    fig.update_yaxes(linewidth=1, linecolor='black',
                     showline=True, showgrid=True, gridcolor='black', gridwidth=0.1,
                     title_font={"size": 40}, tickfont=dict(size=35))

    return fig


def plot_bar_time_to_response(height: int, width: int, x_data: list, y_data: list,
             title: str, y_axis_title: str, bar_color: str, percentage,
             y_error_data=None, added_data=None) -> go.Figure:


    if percentage:
        fig = go.Figure(layout={
            'yaxis': {'title': y_axis_title, 'ticksuffix': '%'}
        })
    else:
        fig = go.Figure(layout={
            'yaxis': {'title': y_axis_title}
        })

    if y_error_data is None:
        fig.add_trace(go.Bar(x=x_data, y=y_data, text=y_data, marker={'color': bar_color}))
    else:
        fig.add_trace(go.Bar(x=x_data, y=y_data, text=y_data, marker={'color': bar_color},
                             error_y=dict(type='data', array=y_error_data)))


    fig.update_layout(height=height, width=width, title={
                                                           'text': title,
                                                            'y': 0.95,
                                                            'x': 0.5,
                                                            'xanchor': 'center',
                                                            'yanchor': 'top'}, plot_bgcolor='rgba(0,0,0,0)')
    if y_error_data is not None and added_data is not None:
        i = 0
        text_height = 7
        for x_val in x_data:
            fig.add_annotation(
                x=x_val,
                y=text_height+38,
                text=f"{y_data[i]:.1f}",
                showarrow=False,
                font=(dict(color='white', size=25))
            )

            fig.add_annotation(
                x=x_val,
                y=text_height,
                text=f"{added_data[i]:.0f} VPs",
                showarrow=False,
                font=(dict(color='white', size=25))
            )

            i = i + 1

    fig.update_traces(texttemplate='%{text:.0f}', textposition='none', textfont=dict(size=22))

    fig.update_xaxes(linewidth=1, linecolor='black', showline=True,
                     showgrid=True, gridcolor='black', gridwidth=0.1,
                     title_font={"size": 25}, tickfont=dict(size=22))

    fig.update_yaxes(linewidth=1, linecolor='black',
                     showline=True, showgrid=True, gridcolor='black', gridwidth=0.1,
                     title_font={"size": 35}, tickfont=dict(size=32))

    return fig


def plot_bar_ranking(height: int, width: int, x_data: list, y_data: list,
             title: str, y_axis_title: str, bar_color: str, percentage,
             y_error_data=None) -> go.Figure:


    if percentage:
        fig = go.Figure(layout={
            'yaxis': {'title': y_axis_title, 'ticksuffix': '%'}
        })
    else:
        fig = go.Figure(layout={
            'yaxis': {'title': y_axis_title}
        })
    #fig = go.Figure()

    if y_error_data is None:
        fig.add_trace(go.Bar(x=x_data, y=y_data, text=y_data, marker={'color': bar_color}))
    else:
        fig.add_trace(go.Bar(x=x_data, y=y_data, text=y_data, marker={'color': bar_color},
                             error_y=dict(type='data', array=y_error_data)))


    fig.update_layout(height=height, width=width, title={
                                                           'text': title,
                                                            'y': 0.95,
                                                            'x': 0.5,
                                                            'xanchor': 'center',
                                                            'yanchor': 'top'}, plot_bgcolor='rgba(0,0,0,0)')
    if y_error_data is not None:
        i = 0
        text_height = 3
        for x_val in x_data:
            fig.add_annotation(
                x=x_val,
                y=text_height,
                text=f"{y_data[i]:.2f}",
                showarrow=False,
                font=(dict(color='white', size=25))
            )
            i = i + 1

    fig.update_traces(texttemplate='%{text:.0f}', textposition='outside', textfont=dict(size=22))

    fig.update_xaxes(linewidth=1, linecolor='black', showline=True,
                     showgrid=True, gridcolor='black', gridwidth=0.1,
                     title_font={"size": 25}, tickfont=dict(size=22))

    fig.update_yaxes(linewidth=1, linecolor='black',
                     showline=True, showgrid=True, gridcolor='black', gridwidth=0.1,
                     title_font={"size": 25}, tickfont=dict(size=22), range=[0, 25])

    return fig


def plot_bar_annotation_over(height: int, width: int, x_data: list, y_data: list,
             title: str, y_axis_title: str, bar_color: str, percentage,
             y_error_data=None) -> go.Figure:


    if percentage:
        fig = go.Figure(layout={
            'yaxis': {'title': y_axis_title, 'ticksuffix': '%'}
        })
    else:
        fig = go.Figure(layout={
            'yaxis': {'title': y_axis_title}
        })
    #fig = go.Figure()

    if y_error_data is None:
        fig.add_trace(go.Bar(x=x_data, y=y_data, text=y_data, marker={'color': bar_color}))
    else:
        fig.add_trace(go.Bar(x=x_data, y=y_data, text=y_data, marker={'color': bar_color},
                             error_y=dict(type='data', array=y_error_data)))


    fig.update_layout(height=height, width=width, title={
                                                           'text': title,
                                                            'y': 0.95,
                                                            'x': 0.5,
                                                            'xanchor': 'center',
                                                            'yanchor': 'top'}, plot_bgcolor='rgba(0,0,0,0)')
    if y_error_data is not None:
        i = 0
        text_height = 15
        for x_val in x_data:
            if i == 0:
                text_height = 15
            elif i == 1:
                text_height = 17
            elif i == 2:
                text_height = 25
            elif i == 5:
                text_height = 29
            elif i == 7:
                text_height = 33
            elif i == 8:
                text_height = 35
            elif i == 10:
                text_height = 38

            fig.add_annotation(
                x=x_val,
                y=-text_height,
                text=f"{y_data[i]:.1f}",
                showarrow=False,
                font=(dict(color='black', size=22))
            )
            i = i + 1


    if percentage:
        fig.update_traces(texttemplate='%{text:.2f}%', textposition='none', textfont=dict(size=22))
    else:
        fig.update_traces(texttemplate='%{text:.2f}', textposition='none', textfont=dict(size=22))


    fig.update_xaxes(linewidth=1, linecolor='black', showline=True,
                     showgrid=True, gridcolor='black', gridwidth=0.1,
                     title_font={"size": 25}, tickfont=dict(size=22))

    fig.update_yaxes(linewidth=1, linecolor='black',
                     showline=True, showgrid=True, gridcolor='black', gridwidth=0.1,
                     title_font={"size": 19}, tickfont=dict(size=22))

    return fig


def plot_bar_annotation_basis(height: int, width: int, x_data: list, y_data: list,
             title: str, y_axis_title: str, bar_color: str, percentage,
             y_error_data=None) -> go.Figure:


    if percentage:
        fig = go.Figure(layout={
            'yaxis': {'title': y_axis_title, 'ticksuffix': '%'}
        })
    else:
        fig = go.Figure(layout={
            'yaxis': {'title': y_axis_title}
        })
    #fig = go.Figure()

    if y_error_data is None:
        fig.add_trace(go.Bar(x=x_data, y=y_data, text=y_data, marker={'color': bar_color}))
    else:
        fig.add_trace(go.Bar(x=x_data, y=y_data, text=y_data, marker={'color': bar_color},
                             error_y=dict(type='data', array=y_error_data)))


    fig.update_layout(height=height, width=width, title={
                                                           'text': title,
                                                            'y': 0.95,
                                                            'x': 0.5,
                                                            'xanchor': 'center',
                                                            'yanchor': 'top'}, plot_bgcolor='rgba(0,0,0,0)')
    if y_error_data is not None:
        i = 0
        text_height = 11
        for x_val in x_data:
            fig.add_annotation(
                x=x_val,
                y=text_height,
                textangle=-90,
                text=f"{y_data[i]:.1f}",
                showarrow=False,
                font=(dict(color='white', size=22))
            )
            i = i + 1


    if percentage:
        fig.update_traces(texttemplate='%{text:.2f}%', textposition='none', textfont=dict(size=22))
    else:
        fig.update_traces(texttemplate='%{text:.2f}', textposition='none', textfont=dict(size=22))


    fig.update_xaxes(linewidth=1, linecolor='black', showline=True,
                     showgrid=True, gridcolor='black', gridwidth=0.1,
                     title_font={"size": 25}, tickfont=dict(size=22))

    fig.update_yaxes(linewidth=1, linecolor='black',
                     showline=True, showgrid=True, gridcolor='black', gridwidth=0.1,
                     title_font={"size": 25}, tickfont=dict(size=22))

    return fig



def plot_bar(height: int, width: int, x_data: list, y_data: list,
             title: str, y_axis_title: str, bar_color: str, percentage,
             y_error_data=None) -> go.Figure:


    if percentage:
        fig = go.Figure(layout={
            'yaxis': {'title': y_axis_title, 'ticksuffix': '%'}
        })
    else:
        fig = go.Figure(layout={
            'yaxis': {'title': y_axis_title}
        })
    #fig = go.Figure()

    if y_error_data is None:
        fig.add_trace(go.Bar(x=x_data, y=y_data, text=y_data, marker={'color': bar_color}))
    else:
        fig.add_trace(go.Bar(x=x_data, y=y_data, text=y_data, marker={'color': bar_color},
                             error_y=dict(type='data', array=y_error_data)))


    fig.update_layout(height=height, width=width, title={
                                                           'text': title,
                                                            'y': 0.95,
                                                            'x': 0.5,
                                                            'xanchor': 'center',
                                                            'yanchor': 'top'}, plot_bgcolor='rgba(0,0,0,0)')
    if y_error_data is not None:
        i = 0
        text_height = 3
        for x_val in x_data:
            fig.add_annotation(
                x=x_val,
                y=text_height,
                text=f"{y_data[i]:.2f}",
                showarrow=False,
                font=(dict(color='white', size=25))
            )
            i = i + 1


    if percentage:
        fig.update_traces(texttemplate='%{text:.2f}%', textposition='outside', textfont=dict(size=22))
    else:
        fig.update_traces(texttemplate='%{text:.2f}', textposition='none', textfont=dict(size=22))


    fig.update_xaxes(linewidth=1, linecolor='black', showline=True,
                     showgrid=True, gridcolor='black', gridwidth=0.1,
                     title_font={"size": 25}, tickfont=dict(size=22))

    fig.update_yaxes(linewidth=1, linecolor='black',
                     showline=True, showgrid=True, gridcolor='black', gridwidth=0.1,
                     title_font={"size": 25}, tickfont=dict(size=22))

    return fig


def plot_single_bar(height: int, width: int, bar_name: List[str], x_data: list, y_data: list,
                     y_title, bar_colors: str):
    fig = go.Figure(layout={
        'yaxis2': {'overlaying': 'y', 'side': 'right', 'ticksuffix': ''}})

    fig.add_trace(go.Bar(name=bar_name, x=x_data, y=y_data, text=y_title,
                         marker={'color': bar_colors}))

    fig.update_layout(barmode='group', height=height, width=width, title={
        'y': 0.95,
        'x': 0.5,
        'xanchor': 'center',
        'yanchor': 'top'}, plot_bgcolor='rgba(0,0,0,0)', legend=dict(x=0.8, y=1.1, font=dict(size=25)))

    fig.update_traces(texttemplate='%{text:2s}', textposition='outside', textfont=dict(size=22))
    fig.update_xaxes(linewidth=1, linecolor='black', showline=True, showgrid=True, gridcolor='black', gridwidth=0.05,
                     title_font={"size": 25}, tickfont=dict(size=22))

    fig.update_yaxes(title=y_title, linewidth=1, linecolor='black', showline=True, showgrid=True, gridcolor='black',
                     gridwidth=0.05,
                     # range=[0, 25],
                     tickfont=dict(size=22), title_font={"size": 28})

    return fig


def plot_grouped_bar(height: int, width: int, n_bar: int, bar_names: list, x_data: list, y_data: list,
                     y_title, bar_colors: List[str]) -> go.Figure:

    fig = go.Figure(layout={
        'yaxis2': {'overlaying': 'y', 'side': 'right', 'ticksuffix' : ''}})

    for i in range(n_bar):
        fig.add_trace(go.Bar(name=bar_names[i], x=x_data, y=y_data[i], text=y_data[i], offsetgroup=i,
                             marker={'color' : bar_colors[i]}))

    fig.update_layout(barmode='group', height=height, width=width, title={
        'y': 0.95,
        'x': 0.5,
        'xanchor': 'center',
        'yanchor': 'top'}, plot_bgcolor='rgba(0,0,0,0)', legend=dict(x=0.7, y=1.1, font=dict(size = 37)))

    fig.update_traces(texttemplate='%{text:2s}', textposition='outside', textfont=dict(size=28))
    fig.update_xaxes(linewidth=1, linecolor='black', showline=True, showgrid=True, gridcolor='black', gridwidth=0.05,
                     title_font={"size": 40}, tickfont=dict(size=25))

    fig.update_yaxes(title=y_title, linewidth=1, linecolor='black', showline=True, showgrid=True, gridcolor='black',
                     gridwidth=0.05,
                     # range=[0, 25],
                     tickfont=dict(size=26), title_font={"size": 30})

    return fig


def plot_double_axis_bar(height: int, width: int, bar_names: list, x_data: list, y_data: list,
                y1_title: str, y2_title: str, bar_colors: list, bar_text_size: int = 10) -> go.Figure:

    fig = go.Figure(layout={
        'yaxis': {'title': y1_title, 'ticksuffix' : '%'},
        'yaxis2': {'title': y2_title, 'overlaying': 'y', 'side': 'right', 'ticksuffix' : '%'}
    })

    y_data_text = []
    for i in y_data:
        lst = []
        for j in i:
            if j > 95:
                lst.append(" ")
            else:
                lst.append(str(round(j, 2))+"%")
        y_data_text.append(lst)

    fig.add_trace(go.Bar(name=bar_names[0], x=x_data, y=y_data[0], yaxis='y',
                                     text=y_data_text[0], offsetgroup=1, marker={'color' : bar_colors[0]}))

    fig.add_trace(go.Bar(name=bar_names[1], x=x_data, y=y_data[1], yaxis='y2',
                                     text=y_data_text[1], offsetgroup=2, marker={'color':bar_colors[1]}))

    fig.update_layout(showlegend=True, barmode='group', height=height, width=width, title={
        'font': {'size': bar_text_size},
        'y': 0.95,
        'x': 0.5,
        'xanchor': 'center',
        'yanchor': 'top'}, plot_bgcolor='rgba(0,0,0,0)', legend=dict(x=0.8, y=1.1, font=dict(size = 25)))

    fig.update_traces(textposition='outside', texttemplate='%{text}', textfont=dict(size=bar_text_size))

    fig.update_xaxes(linewidth=0.5, linecolor='black', showline=True, showgrid=True, gridcolor='black',
                     gridwidth=0.05, title_font = {"size": bar_text_size},
                     tickfont=dict(size=bar_text_size))
    fig.update_yaxes(linewidth=0.5, linecolor='black', showline=True, showgrid=True,
                     gridcolor='black', gridwidth=0.05, title_font = {"size": bar_text_size},
                     tickfont=dict(size=bar_text_size))

    return fig


def plot_multiline_tumour_over_time(height: int, width: int, n_lines: int, y_data, x_data,
                                    x_title: str, optimal: bool) -> go.Figure:

    tickvals = [i for i in range(0, 401, 28)]
    tickvals_y = [k for k in range(-40, 101, 20)]

    if optimal:
        range_x = [-40, 100]
    else:
        range_x = [0, 100]

    fig = go.Figure()
    for i in range(n_lines):
        fig.add_trace(go.Scatter(x=x_data, y=y_data[i],
                                 mode='lines'))

    fig.update_xaxes(title=x_title, linewidth=1, linecolor='black', showline=True, title_font={"size": 30},
                     tickfont=dict(size=25))
    fig.update_yaxes(title="", linewidth=1, linecolor='black', showline=True, range=range_x,
                     title_font={"size": 15},
                     tickfont=dict(size=25))
    fig.update_layout(xaxis=dict(
        tickmode='array',
        tickvals=tickvals,
        ticktext=[str(int(i / 28)) for i in tickvals]
    ), yaxis=dict(
        tickmode='array',
        tickvals=tickvals_y,
        ticktext=[str(i) + "%" for i in tickvals_y]
    ), plot_bgcolor='rgba(0,0,0,0)', height=height, width=width,
        title={
            'y': 0.9,
            'x': 0.5,
            'xanchor': 'center',
            'yanchor': 'top'}, showlegend=False)

    return fig


def plot_double_axis_bar_administrations(height: int, width: int, bar_names: list, x_data: list, y_data: list,
                         y1_title: str, y2_title: str, bar_colors: list, legend: bool) -> go.Figure:

    fig = go.Figure(layout={
        'yaxis': {'title': y1_title, 'ticksuffix': '%', 'range': [0, 100]},
        'yaxis2': {'title': y2_title, 'overlaying': 'y', 'side': 'right', 'ticksuffix': '%', 'range': [0, 100]}
    })

    fig.add_trace(
    go.Bar(name=bar_names[0], x=x_data, y=y_data[0], yaxis='y', offsetgroup=1, marker={'color': bar_colors[0]}))
    fig.add_trace(go.Bar(name=bar_names[1], x=x_data, y=y_data[1], yaxis='y2', offsetgroup=2,
                             marker={'color': bar_colors[1]}))

    tickvals = [i for i in range(0, 58)]
    ticktext = []
    for i in tickvals:
        if i % 4 == 3:
            ticktext.append(str(int(i/4)+1))
        else:
            ticktext.append(" ")

    fig.update_layout(barmode='group', height=height, width=width, title={
        'y': 0.95,
        'x': 0.5,
        'xanchor': 'center',
        'yanchor': 'top'}, plot_bgcolor='rgba(0,0,0,0)', legend=dict(x=0.9, y=-0.5),
                      font=dict(size=18), xaxis=dict( tickmode='array', tickvals=tickvals,
                                           ticktext=ticktext), showlegend=legend)

    fig.update_traces(textposition='outside')
    fig.update_xaxes(linewidth=0.5, linecolor='black', showline=True, showgrid=True, gridcolor='black', gridwidth=0.05,
                     title="Time (months)", tickfont=dict(size=27), title_font={"size": 35})
    fig.update_yaxes(linewidth=0.5, linecolor='black', showline=True, showgrid=True,
                     gridcolor='black', gridwidth=0.05, rangemode="tozero",tickfont=dict(size=27))

    return fig


def save_plot(plot_fig: go.Figure, name_path: str) -> None:
    plot_fig.write_image(name_path)
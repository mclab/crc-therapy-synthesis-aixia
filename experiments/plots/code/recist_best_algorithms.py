import json

from lib.plot_lib import save_plot, plot_grouped_bar
from lib.input_lib import get_command_line_arguments_recist

import pandas as pd
from typing import Dict

from palettable.cubehelix import Cubehelix


def get_recist_classification(init_tum: float, end_tum: float) -> str:
    recist = (end_tum - init_tum) / init_tum * 100
    if recist < -30 and end_tum < 10:
        return "CR"
    elif recist < -30 and end_tum >= 10:
        return "PR"
    elif recist > 20:
        return "PD"
    else:
        return "SD"


def calculate_recist_treatment(optimal_data, population_1_size, frame_population1: pd.DataFrame,
                               population_2_size, frame_population2: pd.DataFrame) -> Dict[str, int]:

    recist_classification_counter = dict()
    for i in ['CR', 'PR', 'SD', 'PD']:
        recist_classification_counter[i] = 0

    vps = optimal_data['population_1']
    for i in range(population_1_size):
        vp_row = frame_population1.iloc[i]
        vp_optimal = vps[f'virtual_patient_{i}']
        classification = get_recist_classification(vp_row['initial_tumour_diameter'], vp_optimal['tumour_diameter'][-1])
        recist_classification_counter[classification] += 1

    vps = optimal_data['population_2']
    for i in range(population_2_size):
        vp_row = frame_population2.iloc[i]
        vp_optimal = vps[f'virtual_patient_{i}']
        classification = get_recist_classification(vp_row['initial_tumour_diameter'], vp_optimal['tumour_diameter'][-1])
        recist_classification_counter[classification] += 1

    return recist_classification_counter


def plot_recist_classification(plot_filename: str, reference_counter, scatter_counter, genetic_counter, differential_counter,
                          particle_counter, hooke_counter) -> None:

    optimal_scatter = [scatter_counter['CR'], scatter_counter['PR'], scatter_counter['SD'], scatter_counter['PD']]
    optimal_scatter.reverse()

    optimal_genetic = [genetic_counter['CR'], genetic_counter['PR'], genetic_counter['SD'], genetic_counter['PD']]
    optimal_genetic.reverse()

    optimal_differential = [differential_counter['CR'], differential_counter['PR'], differential_counter['SD'],
                         differential_counter['PD']]
    optimal_differential.reverse()

    optimal_particle = [particle_counter['CR'], particle_counter['PR'], particle_counter['SD'], particle_counter['PD']]
    optimal_particle.reverse()

    optimal_hooke = [hooke_counter['CR'], hooke_counter['PR'], hooke_counter['SD'], hooke_counter['PD']]
    optimal_hooke.reverse()

    reference = [reference_counter['CR'], reference_counter['PR'], reference_counter['SD'], reference_counter['PD']]
    reference.reverse()

    data =  [ [elem for elem in reference],
              [elem for elem in optimal_scatter],
              [elem for elem in optimal_genetic],
              [elem for elem in optimal_differential],
              [elem for elem in optimal_particle],
              [elem for elem in optimal_hooke]]

    names = ['Progressive Disease', 'Stable Disease', 'Partial Response', 'Complete Response']

    palette = Cubehelix.make(start=0.4, rotation=-0.5, n=16)
    colors = [f"rgb({c[0]},{c[1]},{c[2]})" for c in palette.colors]

    plot = plot_grouped_bar(790, 1900, 6,
                            ['Reference', 'Scatter Search', 'Genetic Algorithm', 'Differential Evolution', 'Particle Swarm',
                             'Hooke & Jeeves'],
                            names, data, '# Patients', [
                                colors[2], colors[5], colors[7], colors[9], colors[11], colors[13]
                            ])

    save_plot(plot, plot_filename)


def main() -> None:
    optimisation_data_path, reference_data_path, plot_out_path,\
        population_1_path, population_2_path = get_command_line_arguments_recist()

    # Populations size
    population_1_size = 11
    population_2_size = 14

    # Loading population data
    population1_frame = pd.read_csv(population_1_path, delimiter='\t')
    population2_frame = pd.read_csv(population_2_path, delimiter='\t')

    # Simulation data loading
    with open(optimisation_data_path, 'r') as f:
        loaded_optimisation_data = json.load(f)

    with open(reference_data_path, 'r') as f:
        loaded_reference_data = json.load(f)

    # Recist classification
    recist_initial_classification_reference = calculate_recist_treatment(loaded_reference_data,
                                                                         population_1_size, population1_frame,
                                                                         population_2_size, population2_frame)

    recist_optimal_classification_scatter = calculate_recist_treatment(
                                                                        loaded_optimisation_data['scatter_search'],
                                                                        population_1_size, population1_frame,
                                                                        population_2_size, population2_frame)

    recist_optimal_classification_genetic = calculate_recist_treatment(
                                                            loaded_optimisation_data['genetic_algorithm'],
                                                            population_1_size, population1_frame,
                                                            population_2_size, population2_frame)

    recist_optimal_classification_differential = calculate_recist_treatment(
                                                            loaded_optimisation_data['differential_evolution'],
                                                            population_1_size, population1_frame,
                                                            population_2_size, population2_frame)

    recist_optimal_classification_particle = calculate_recist_treatment(
                                                        loaded_optimisation_data['particle_swarm'],
                                                        population_1_size, population1_frame,
                                                        population_2_size, population2_frame)

    recist_optimal_classification_hooke = calculate_recist_treatment(
                                                 loaded_optimisation_data['hooke_jeeves'],
                                                 population_1_size, population1_frame,
                                                 population_2_size, population2_frame)

    # Plot recist classification
    plot_recist_classification(plot_out_path, recist_initial_classification_reference,
                               recist_optimal_classification_scatter,
                               recist_optimal_classification_genetic, recist_optimal_classification_differential,
                               recist_optimal_classification_particle, recist_optimal_classification_hooke)


if __name__ == '__main__':
    main()
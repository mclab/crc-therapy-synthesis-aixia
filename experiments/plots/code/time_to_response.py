from typing import List

from lib.plot_lib import save_plot, plot_bar_time_to_response
from lib.input_lib import get_command_line_arguments_time_to_response
import json

from palettable.cubehelix import Cubehelix



def build_time_to_response_plot(plot_filename: str, kpis_analysis_data_path: str):
    with open(kpis_analysis_data_path, 'r') as f:
        kpi_data_analysis = json.load(f)

    algorithms = ['reference', 'genetic_algorithm', 'differential_evolution', 'hooke_jeeves',
                  'scatter_search', 'steepest_descent', 'praxis', 'particle_swarm',
                  'nelder_mead',  'evolutionary_programming', 'random_search',
                  'evolutionary_strategy_sr']

    names_plot = ['Ref.', 'Gen. Alg.', 'Diff. Ev.', 'H. & J.', 'Scat. S.',
                  'Steep. D.', 'Praxis',  'Par. Sw.', 'Neld. M.',
                  'Ev. Prog.', 'Rand. S.', 'SRES']


    time_to_response_algorithms = kpi_data_analysis['time_to_response']
    mean_time_to_response = [time_to_response_algorithms[algo]['mean'] for algo in algorithms]
    error_time_to_response = [time_to_response_algorithms[algo]['std'] for algo in algorithms]
    vps_time_to_response = [time_to_response_algorithms[algo]['num_vps'] for algo in algorithms]

    plot_time_to_response_bar(plot_filename, names_algrithms=names_plot, mean_time_to_response=mean_time_to_response,
                              error_time_to_response=error_time_to_response, vps_time_to_response=vps_time_to_response)


def plot_time_to_response_bar(plot_filename: str, names_algrithms, mean_time_to_response, error_time_to_response,
                     vps_time_to_response) -> None:

    palette = Cubehelix.make(start=0.3, rotation=-0.5, n=16)
    colors = [f"rgb({c[0]},{c[1]},{c[2]})" for c in palette.colors]

    plot1 = plot_bar_time_to_response(700, 2200, title="", x_data=names_algrithms, y_data=mean_time_to_response,
                                  y_axis_title='Time to Response (Days)', y_error_data=error_time_to_response,
                                  bar_color=colors[8], percentage=False,
                                      added_data=vps_time_to_response
                                      )

    save_plot(plot1, plot_filename)


def main():
    kpi_data_path, out_path_plot = get_command_line_arguments_time_to_response()

    # build plot
    build_time_to_response_plot(out_path_plot, kpi_data_path)

if __name__ == '__main__':
    main()
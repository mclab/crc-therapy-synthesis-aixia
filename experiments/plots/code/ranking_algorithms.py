from typing import List

from lib.plot_lib import save_plot, plot_bar_ranking
from lib.input_lib import get_command_line_arguments_ranking_algorithm
import json

from palettable.cubehelix import Cubehelix


def process_result_analysis_json(analysis_result_json_path: str):
    with open(analysis_result_json_path, 'r') as f:
        analysis_result_json = json.load(f)

    analysis_data = dict()
    for algo in list(analysis_result_json['ranking'].keys()):
        analysis_data[algo] = analysis_result_json['ranking'][algo]

    return analysis_data


def plot_ranking_algorithms(plot_filename: str, names_plot_ranking: List[str], data_ranking: List[float]) -> None:
    palette = Cubehelix.make(start=0.3, rotation=-0.5, n=16)
    colors = [f"rgb({c[0]},{c[1]},{c[2]})" for c in palette.colors]
    plot1 = plot_bar_ranking(400, 850, title="", x_data=names_plot_ranking, y_data=data_ranking,
                     y_axis_title='# Patients',
                     bar_color=colors[11], percentage=False, y_error_data=None)

    save_plot(plot1, plot_filename)


def main():
    analysis_data_path, out_path_plot = get_command_line_arguments_ranking_algorithm()

    analysis_data = process_result_analysis_json(analysis_data_path)

    names_plot_ranking = ['Particle Swarm', 'Scatter Search', 'Hooke & Jeeves']

    data_ranking = [analysis_data['particle_swarm'],
                    analysis_data['scatter_search'],
                    analysis_data['hooke_jeeves']]

    plot_ranking_algorithms(out_path_plot, names_plot_ranking, data_ranking)


if __name__ == '__main__':
    main()
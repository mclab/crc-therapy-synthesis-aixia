from typing import List

from lib.plot_lib import save_plot, plot_bar_annotation_basis, plot_bar_annotation_over
from lib.input_lib import get_frame_data, get_command_line_arguments_drug_inefficacy
from statistics import mean
import json
import numpy as np

from palettable.cubehelix import Cubehelix


def process_experiments_data(initial_treatment_path, all_data_path):

    population_1_size = 11
    population_2_size = 14
    initial_treatment_frame = get_frame_data(initial_treatment_path)

    with open(all_data_path, 'r') as f:
        data_experiments = json.load(f)

    algorithms = ['reference', 'differential_evolution', 'evolutionary_programming', 'evolutionary_strategy_sr',
                  'hooke_jeeves', 'praxis', 'scatter_search', 'steepest_descent',
                  'random_search', 'nelder_mead', 'genetic_algorithm', 'particle_swarm']

    optimal_data = dict()
    for algo in algorithms:
        optimal_data[algo] = {'total_drugs': [], 'tumour': []}

    total_drug = 100
    for algo in algorithms:
        for i in range(population_1_size):
            if algo != 'reference':
                optimal_total_drugs = data_experiments[algo]['population_1'][f'virtual_patient_{i}']['total_drugs']
                optimal_data[algo]['total_drugs'].append(
                    -((optimal_total_drugs - total_drug) / total_drug * 100)
                )
                tumour_diameter = data_experiments[algo]['population_1'][f'virtual_patient_{i}']['tumour_diameter'][-1]
            else:
                tumour_diameter = initial_treatment_frame.iloc[i]['final_tumour_diameter']

            initial_tumour_diameter = initial_treatment_frame.iloc[i]['initial_tumour_diameter']
            optimal_data[algo]['tumour'].append(
               ((tumour_diameter - initial_tumour_diameter) / initial_tumour_diameter * 100)
            )

        for j in range(population_2_size):
            if algo != 'reference':
                optimal_total_drugs = data_experiments[algo]['population_2'][f'virtual_patient_{j}']['total_drugs']
                optimal_data[algo]['total_drugs'].append(
                    -((optimal_total_drugs - total_drug) / total_drug * 100)
                )
                tumour_diameter = data_experiments[algo]['population_2'][f'virtual_patient_{j}']['tumour_diameter'][-1]
            else:
                tumour_diameter = initial_treatment_frame.iloc[population_1_size+j]['final_tumour_diameter']

            initial_tumour_diameter = initial_treatment_frame.iloc[population_1_size+j]['initial_tumour_diameter']
            optimal_data[algo]['tumour'].append(
                ((tumour_diameter - initial_tumour_diameter) / initial_tumour_diameter * 100)
            )
    return optimal_data


def plot_double_axis(plot1_filename: str, plot2_filename: str,  names_plot_drug, plot_drugs: List[float],
                     y_error_data_drugs, names_plot_inefficacy, plot_inefficacy: List[float],
                     y_error_data_inefficacy) -> None:


    palette = Cubehelix.make(start=0.3, rotation=-0.5, n=16)
    colors = [f"rgb({c[0]},{c[1]},{c[2]})" for c in palette.colors]
    plot1 = plot_bar_annotation_basis(500, 1000, title="", x_data=names_plot_drug, y_data=plot_drugs,
                                      y_axis_title='Mean Drugs Saving',
                                      bar_color=colors[9], percentage=True, y_error_data=y_error_data_drugs)

    plot2 = plot_bar_annotation_over(500, 1000, title='',
                      x_data=names_plot_inefficacy, y_data=plot_inefficacy, y_axis_title='Mean Final Tumour Size Reduction',
                     bar_color=colors[5], percentage=True, y_error_data=y_error_data_inefficacy)

    save_plot(plot1, plot1_filename)
    save_plot(plot2, plot2_filename)


def main():
    all_data_path, reference_path, out_path_plot1, out_path_plot2 = get_command_line_arguments_drug_inefficacy()

    optimal_data = process_experiments_data(reference_path, all_data_path)

    # Plot inverse drugs
    names_plot_drug = ['Neld. M.', 'Steep. D.', 'Praxis', 'Rand. S.',
                    'SRES', 'Ev. Prog.', 'Diff. Ev.',
                    'Gen. Alg.', 'Par. Sw.', 'Scat. S.', 'H. & J.']

    data_drugs = [mean(optimal_data['nelder_mead']['total_drugs']),
                  mean(optimal_data['steepest_descent']['total_drugs']),
                  mean(optimal_data['praxis']['total_drugs']),
                  mean(optimal_data['random_search']['total_drugs']),
                  mean(optimal_data['evolutionary_strategy_sr']['total_drugs']),
                  mean(optimal_data['evolutionary_programming']['total_drugs']),
                  mean(optimal_data['differential_evolution']['total_drugs']),
                  mean(optimal_data['genetic_algorithm']['total_drugs']),
                  mean(optimal_data['particle_swarm']['total_drugs']),
                  mean(optimal_data['scatter_search']['total_drugs']),
                  mean(optimal_data['hooke_jeeves']['total_drugs'])
                ]

    data_error_drugs = [np.std(optimal_data['nelder_mead']['total_drugs'], ddof=1) / np.sqrt(np.size(optimal_data['nelder_mead']['total_drugs'])),
                        np.std(optimal_data['steepest_descent']['total_drugs'], ddof=1) / np.sqrt(np.size(optimal_data['steepest_descent']['total_drugs'])),
                        np.std(optimal_data['praxis']['total_drugs'], ddof=1) / np.sqrt(np.size(optimal_data['praxis']['total_drugs'])),
                        np.std(optimal_data['random_search']['total_drugs'], ddof=1) / np.sqrt(np.size(optimal_data['random_search']['total_drugs'])),
                        np.std(optimal_data['evolutionary_strategy_sr']['total_drugs'], ddof=1) / np.sqrt(np.size(optimal_data['evolutionary_strategy_sr']['total_drugs'])),
                        np.std(optimal_data['evolutionary_programming']['total_drugs'], ddof=1) / np.sqrt(np.size(optimal_data['evolutionary_programming']['total_drugs'])),
                        np.std(optimal_data['differential_evolution']['total_drugs'], ddof=1) / np.sqrt(np.size(optimal_data['differential_evolution']['total_drugs'])),
                        np.std(optimal_data['genetic_algorithm']['total_drugs'], ddof=1) / np.sqrt(np.size(optimal_data['genetic_algorithm']['total_drugs'])),
                        np.std(optimal_data['particle_swarm']['total_drugs'], ddof=1) / np.sqrt(np.size(optimal_data['particle_swarm']['total_drugs'])),
                        np.std(optimal_data['scatter_search']['total_drugs'], ddof=1) / np.sqrt(np.size(optimal_data['scatter_search']['total_drugs'])),
                        np.std(optimal_data['hooke_jeeves']['total_drugs'], ddof=1) / np.sqrt(np.size(optimal_data['hooke_jeeves']['total_drugs']))]


    # Plot percentage tumour decrement
    names_plot_inefficacy = ['Ref.', 'Praxis', 'Steep. D.', 'Neld. M.', 'SRES',
                             'Rand. S.', 'Ev. Prog.', 'Scat S.',
                            'Gen. Alg.', 'Par. Sw.', 'Diff. Ev.', 'H. & J.']

    data_inefficacy = [mean(optimal_data['reference']['tumour']),
                       mean(optimal_data['praxis']['tumour']),
                       mean(optimal_data['steepest_descent']['tumour']),
                       mean(optimal_data['nelder_mead']['tumour']),
                       mean(optimal_data['evolutionary_strategy_sr']['tumour']),
                       mean(optimal_data['random_search']['tumour']),
                       mean(optimal_data['evolutionary_programming']['tumour']),
                       mean(optimal_data['scatter_search']['tumour']),
                       mean(optimal_data['genetic_algorithm']['tumour']),
                       mean(optimal_data['particle_swarm']['tumour']),
                       mean(optimal_data['differential_evolution']['tumour']),
                       mean(optimal_data['hooke_jeeves']['tumour'])
                       ]

    data_error_inefficacy = [np.std(optimal_data['reference']['tumour'], ddof=1) / np.sqrt(np.size(optimal_data['reference']['tumour'])),
                             np.std(optimal_data['praxis']['tumour'], ddof=1) / np.sqrt(np.size(optimal_data['praxis']['tumour'])),
                             np.std(optimal_data['steepest_descent']['tumour'], ddof=1) / np.sqrt(np.size(optimal_data['steepest_descent']['tumour'])),
                             np.std(optimal_data['nelder_mead']['tumour'], ddof=1) / np.sqrt(np.size(optimal_data['nelder_mead']['tumour'])),
                             np.std(optimal_data['evolutionary_strategy_sr']['tumour'], ddof=1) / np.sqrt(np.size(optimal_data['evolutionary_strategy_sr']['tumour'])),
                             np.std(optimal_data['random_search']['tumour'], ddof=1) / np.sqrt(np.size(optimal_data['random_search']['tumour'])),
                             np.std(optimal_data['evolutionary_programming']['tumour'], ddof=1) / np.sqrt(np.size(optimal_data['evolutionary_programming']['tumour'])),
                             np.std(optimal_data['scatter_search']['tumour'], ddof=1) / np.sqrt(np.size(optimal_data['scatter_search']['tumour'])),
                             np.std(optimal_data['genetic_algorithm']['tumour'], ddof=1) / np.sqrt(np.size(optimal_data['genetic_algorithm']['tumour'])),
                             np.std(optimal_data['particle_swarm']['tumour'], ddof=1) / np.sqrt(
                                 np.size(optimal_data['particle_swarm']['tumour'])),
                             np.std(optimal_data['differential_evolution']['tumour'], ddof=1) / np.sqrt(np.size(optimal_data['differential_evolution']['tumour'])),
                             np.std(optimal_data['hooke_jeeves']['tumour'], ddof=1) / np.sqrt(
                                 np.size(optimal_data['hooke_jeeves']['tumour']))]

    plot_double_axis(out_path_plot1, out_path_plot2,  names_plot_drug, data_drugs, data_error_drugs,
                        names_plot_inefficacy, data_inefficacy, data_error_inefficacy)


if __name__ == '__main__':
    main()

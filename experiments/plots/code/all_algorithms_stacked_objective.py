from typing import List, Dict

from lib.plot_lib import save_plot, objective_stacked_bar
from lib.input_lib import get_command_line_arguments_stacked_objective
from statistics import mean
import json

from palettable.cubehelix import Cubehelix


def process_experiments_data(all_data_path: str) -> Dict[str, Dict[str, List[float]]]:
    population_1_size = 11
    population_2_size = 14

    with open(all_data_path, 'r') as f:
        data_experiments = json.load(f)

    algorithms = ['differential_evolution', 'evolutionary_programming', 'evolutionary_strategy_sr',
                  'hooke_jeeves', 'praxis', 'scatter_search', 'steepest_descent',
                  'random_search', 'nelder_mead', 'genetic_algorithm', 'particle_swarm']

    optimal_data: Dict[str, Dict[str, List[float]]] = dict()
    for algo in algorithms:
        optimal_data[algo] = {'total_drugs': [], 'inefficacy': []}

        for i in range(population_1_size):
            optimal_data[algo]['total_drugs'].append(
                data_experiments[algo]['population_1'][f'virtual_patient_{i}']['total_drugs'] * 0.1
            )
            optimal_data[algo]['inefficacy'].append(
                data_experiments[algo]['population_1'][f'virtual_patient_{i}']['inefficacy'] * 0.9
            )

        for j in range(population_2_size):
            optimal_data[algo]['total_drugs'].append(
                data_experiments[algo]['population_2'][f'virtual_patient_{j}']['total_drugs'] * 0.1
            )
            optimal_data[algo]['inefficacy'].append(
                data_experiments[algo]['population_2'][f'virtual_patient_{j}']['inefficacy'] * 0.9
            )

    return optimal_data


def plot_double_axis(plot_filename, names_plot_objective, stacked_data_objective) -> None:

    palette = Cubehelix.make(start=0.3, rotation=-0.5, n=16)
    colors = [f"rgb({c[0]},{c[1]},{c[2]})" for c in palette.colors]

    plot = objective_stacked_bar(870, 2200, title="", x_data=names_plot_objective, y_data=stacked_data_objective,
                                  y_axis_title='Mean Objective Value', legend=['Inefficacy', 'Drugs Amount'],
                                  bar_color=[colors[5], colors[9]], percentage=False, y_error_data=None)

    save_plot(plot, plot_filename)


def main():
    all_data_path, out_path_plot= get_command_line_arguments_stacked_objective()

    optimal_data = process_experiments_data(all_data_path)

    '''
    data_error_objective = [np.std(optimal_data['praxis']['objective_value'], ddof=1) / np.sqrt(
                                        np.size(optimal_data['praxis']['objective_value'])),
                             np.std(optimal_data['steepest_descent']['objective_value'], ddof=1) / np.sqrt(
                                        np.size(optimal_data['steepest_descent']['objective_value'])),
                             np.std(optimal_data['nelder_mead']['objective_value'], ddof=1) / np.sqrt(
                                        np.size(optimal_data['nelder_mead']['objective_value'])),
                             np.std(optimal_data['random_search']['objective_value'], ddof=1) / np.sqrt(
                                        np.size(optimal_data['random_search']['objective_value'])),
                             np.std(optimal_data['evolutionary_strategy_sr']['objective_value'], ddof=1) / np.sqrt(
                                        np.size(optimal_data['evolutionary_strategy_sr']['objective_value'])),
                             np.std(optimal_data['evolutionary_programming']['objective_value'], ddof=1) / np.sqrt(
                                        np.size(optimal_data['evolutionary_programming']['objective_value'])),
                            np.std(optimal_data['scatter_search']['objective_value'], ddof=1) / np.sqrt(
                                np.size(optimal_data['scatter_search']['objective_value'])),
                             np.std(optimal_data['genetic_algorithm']['objective_value'], ddof=1) / np.sqrt(
                                        np.size(optimal_data['genetic_algorithm']['objective_value'])),
                             np.std(optimal_data['differential_evolution']['objective_value'], ddof=1) / np.sqrt(
                                        np.size(optimal_data['differential_evolution']['objective_value'])),
                             np.std(optimal_data['particle_swarm']['objective_value'], ddof=1) / np.sqrt(
                                        np.size(optimal_data['particle_swarm']['objective_value'])),
                             np.std(optimal_data['hooke_jeeves']['objective_value'], ddof=1) / np.sqrt(
                                        np.size(optimal_data['hooke_jeeves']['objective_value']))]
                                        
     data_objective = [mean(optimal_data['praxis']['objective_value']),
                      mean(optimal_data['steepest_descent']['objective_value']),
                      mean(optimal_data['nelder_mead']['objective_value']),
                      mean(optimal_data['random_search']['objective_value']),
                      mean(optimal_data['evolutionary_strategy_sr']['objective_value']),
                      mean(optimal_data['evolutionary_programming']['objective_value']),
                      mean(optimal_data['scatter_search']['objective_value']),
                      mean(optimal_data['genetic_algorithm']['objective_value']),
                      mean(optimal_data['differential_evolution']['objective_value']),
                      mean(optimal_data['particle_swarm']['objective_value']),
                      mean(optimal_data['hooke_jeeves']['objective_value'])
                    ]
    '''
    '''
    names_plot_objective = ['Praxis', 'Steepest Descent', 'Nelder Mead', 'Random Search',
                    'Evolutionary Strategy SR', 'Evolutionary Programming', 'Scatter Search',
                    'Genetic Algorithm', 'Differential Evolution', 'Particle Swarm', 'Hooke & Jeeves']
    '''

    names_plot_objective = ['Praxis', 'Steep. D.', 'Neld. M.', 'Rand. S.',
                            'SRES', 'Ev. Prog.', 'Scat S.',
                            'Gen. Alg.', 'Diff. Ev.', 'Par. Sw.', 'H. & J.']

    stacked_data_objective = [
        [
            mean(optimal_data['praxis']['inefficacy']),
            mean(optimal_data['steepest_descent']['inefficacy']),
            mean(optimal_data['nelder_mead']['inefficacy']),
            mean(optimal_data['random_search']['inefficacy']),
            mean(optimal_data['evolutionary_strategy_sr']['inefficacy']),
            mean(optimal_data['evolutionary_programming']['inefficacy']),
            mean(optimal_data['scatter_search']['inefficacy']),
            mean(optimal_data['genetic_algorithm']['inefficacy']),
            mean(optimal_data['differential_evolution']['inefficacy']),
            mean(optimal_data['particle_swarm']['inefficacy']),
            mean(optimal_data['hooke_jeeves']['inefficacy'])
        ],
        [
            mean(optimal_data['praxis']['total_drugs']),
            mean(optimal_data['steepest_descent']['total_drugs']),
            mean(optimal_data['nelder_mead']['total_drugs']),
            mean(optimal_data['random_search']['total_drugs']),
            mean(optimal_data['evolutionary_strategy_sr']['total_drugs']),
            mean(optimal_data['evolutionary_programming']['total_drugs']),
            mean(optimal_data['scatter_search']['total_drugs']),
            mean(optimal_data['genetic_algorithm']['total_drugs']),
            mean(optimal_data['differential_evolution']['total_drugs']),
            mean(optimal_data['particle_swarm']['total_drugs']),
            mean(optimal_data['hooke_jeeves']['total_drugs'])
        ]
    ]

    plot_double_axis(out_path_plot, names_plot_objective, stacked_data_objective)


if __name__ == '__main__':
    main()

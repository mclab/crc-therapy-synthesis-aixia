from apricopt.model.Model import Model
from apricopt.simulation.COPASI.COPASIEngine import COPASIEngine
import json
import pandas as pd

from typing import Dict, List

def get_model(model_path: str, observed_output_path) -> (Model, COPASIEngine):
    with open(observed_output_path, 'r') as f:
        observed_outputs = json.load(f)

    sim_engine = COPASIEngine()
    model = Model(model_filename=model_path, abs_tol=1e-12,
                  rel_tol=1e-6, sim_engine=sim_engine, time_step=1, observed_outputs=observed_outputs)

    return model, sim_engine


def get_population(patients_path: str) -> Dict[int, Dict[str, float]]:
    all_pats = dict()
    frame = pd.read_csv(patients_path, delimiter='\t')
    for index, row in frame.iterrows():
        d = dict()
        for k, v in row.items():
            d[k] = v
        all_pats[index] = d
    return all_pats


def get_initial_treatment(treatment_path: str):
    treats = dict()
    frame = pd.read_csv(treatment_path, delimiter='\t')
    for index, row in frame.iterrows():
        for k, v in row.items():
            if k != "conditionId":
                treats[k] = v
    return treats


def get_reference_treatments(reference_treatments_path: str) -> List[Dict[str, float]]:
    all_treats = []
    frame = pd.read_csv(reference_treatments_path, delimiter='\t')
    for index, row in frame.iterrows():
        tmp = dict()
        for k, v in sorted(row.items(), key=lambda i: i[0]):
            if k != "conditionId":
                tmp[k] = v
        all_treats.append(tmp)
    return all_treats
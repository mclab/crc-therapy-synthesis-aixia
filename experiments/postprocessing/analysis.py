import json
import math
import sys

import numpy as np
from typing import *

MIN_DOSE_ATE = 0.01
MIN_DOSE_CIBI = 0.5


def myround(f):
    if f - math.floor(f) < 0.5:
        return math.floor(f)
    else:
        return math.ceil(f)


def mean_and_std(l: List[float]) -> Tuple[float, float]:
    array = np.array(l)
    mean = np.mean(array)
    std = np.std(array, ddof=1) / np.sqrt(np.size(array))

    return float(mean), float(std)


def algorithm_ranking(data) -> Dict[str, int]:
    vps = data['random_search'].keys()
    rank = dict()
    for vp in vps:
        best_algo = ""
        best_obj = float("Inf")
        for algo_name, algo_data in data.items():
            obj_value = algo_data[vp]['objective_value']
            if obj_value < best_obj:
                best_algo = algo_name
                best_obj = obj_value
        if best_algo in rank:
            rank[best_algo] += 1
        else:
            rank[best_algo] = 1

    return rank


def time_to_response_vp(vp_data: dict) -> int:
    """
	Returns the index of the first day in which the tumour diameter starts shrinking.
	If the tumour never shrinks, returns -1.
	"""
    diameter = vp_data['tumour_diameter']
    resp_day = -1
    for day in range(1, len(diameter)):
        if diameter[day] < diameter[day - 1]:
            resp_day = day
            break
    return resp_day


def mean_time_to_response(algo_data: dict) -> Tuple[Tuple[float, float], int]:
    times = [time_to_response_vp(vp_data) for _, vp_data in algo_data.items()]
    times = [t for t in times if t > 0]
    return mean_and_std(times), len(times)


def final_over_initial(vp_data: dict) -> float:
    """
	Returns the ratio between the final and the initial tumour diameter
	"""
    diameter = vp_data['tumour_diameter']
    return diameter[-1] / diameter[0]


def get_therapy_by_week(vp_data: dict) -> List[Tuple[float, float]]:
    ate_freq = myround(vp_data['ate_drug'][0])
    ate_doses = vp_data['ate_drug'][1:]
    cibi_freq = myround(vp_data['cibi_drug'][0])
    cibi_doses = vp_data['cibi_drug'][1:]

    result = []
    for week in range(58):
        day = week*7
        if week <= 4:
            if week % ate_freq == 0:
                ate_dose = ate_doses[0]
            else:
                ate_dose = 0
            if week % cibi_freq == 0:
                cibi_dose = cibi_doses[0]
            else:
                cibi_dose = 0
        else:
            if week % 4 == 0:
                w = week - 1
            else:
                w = week

            if week % ate_freq == 0:
                ate_dose = ate_doses[math.floor(w/4)]
            else:
                ate_dose = 0

            if week % cibi_freq == 0:
                m_in = math.floor(w/4)
                cibi_dose = cibi_doses[m_in]
            else:
                cibi_dose = 0

        result += [(ate_dose, cibi_dose)]

    return result









def doses_for_vp(vp_data) -> Tuple[List[float], List[float]]:
    therapy = get_therapy_by_week(vp_data)
    doses_ate = []
    doses_cibi = []

    for ate, cibi in therapy:
        if ate >= MIN_DOSE_ATE:
            doses_ate += [ate]
        if cibi >= MIN_DOSE_CIBI:
            doses_cibi += [cibi]

    return doses_ate, doses_cibi


def average_doses_for_vp(vp_data) -> Tuple[float, float]:
    doses_ate, doses_cibi = doses_for_vp(vp_data)
    avg_ate = sum(doses_ate) / len(doses_ate) if len(doses_ate) > 0 else 0
    avg_cibi = sum(doses_cibi) / len(doses_cibi) if len(doses_cibi) > 0 else 0

    return avg_ate, avg_cibi


def max_time_between_administrations(vp_data) -> int:
    therapy = get_therapy_by_week(vp_data)
    period = 1
    max_period = 1
    for ate, cibi in therapy:
        if ate < MIN_DOSE_ATE and cibi < MIN_DOSE_CIBI:
            period += 1
            if period > max_period:
                max_period = period
        else:
            period = 1
    return max_period


'''def distance_between_administrations(vp_data) -> Tuple[List[int], List[int], List[int], List[int]]:
    """
	Returns the distances (in weeks) between consecutive administrations.
	The result is a tuple of four elements: the first is the distances for 
	Atezolizumab, the second is the distances for Cibisatamab and the third
	is the distances between administrations of any of the two drugs, and 
	the fourth is the distances between two administrations of both drugs.
	"""
    therapy = get_treatment_by_day(vp_data)
    weeks_ate = []
    weeks_cibi = []
    weeks_any = []
    weeks_both = []

    for week, adm in enumerate(therapy):
        ate, cibi = adm
        if ate >= MIN_DOSE_ATE or cibi >= MIN_DOSE_CIBI:
            weeks_any += [week]
        if ate >= MIN_DOSE_ATE and cibi >= MIN_DOSE_CIBI:
            weeks_both += [week]
        if ate >= MIN_DOSE_ATE:
            weeks_ate += [week]
        if cibi >= MIN_DOSE_CIBI:
            weeks_cibi += [week]

    distances_ate = []
    distances_cibi = []
    distances_any = []
    distances_both = []

    for i in range(1, len(weeks_any)):
        distances_any += [weeks_any[i] - weeks_any[i - 1]]

    for i in range(1, len(weeks_both)):
        distances_both += [weeks_both[i] - weeks_both[i - 1]]

    for i in range(1, len(weeks_ate)):
        distances_ate += [weeks_ate[i] - weeks_ate[i - 1]]

    for i in range(1, len(weeks_cibi)):
        distances_cibi += [weeks_cibi[i] - weeks_cibi[i - 1]]

    return distances_ate, distances_cibi, distances_any, distances_both
'''

'''
def average_distance_between_administrations(vp_data) -> Tuple[float, float, float, float]:
	"""
	Returns the average distance (in weeks) between consecutive administrations.
	The result is a tuple of three elements: the first is the average distance for 
	Atezolizumab, the second is the average distance for Cibisatamab and the third
	is the average distance considering both drugs.
	"""
	therapy = get_treatment_by_day(vp_data)
	weeks_ate = []
	weeks_cibi = []
	weeks_any = []
	weeks_both = []


	for week, adm in enumerate(therapy):
		if adm != (0,0):
			weeks_any += [week]
		ate, cibi = adm
		if ate > 0 and cibi > 0:
			weeks_both += [week]
		if ate > 0:
			weeks_ate += [week]
		if cibi > 0:
			weeks_cibi += [week]

	distances_ate ,distances_cibi, distances_any, distances_both = distance_between_administrations(vp_data)

	res_ate = sum(distances_ate)/len(distances_ate) if len(distances_ate) > 0 else 0
	res_cibi = sum(distances_cibi)/len(distances_cibi) if len(distances_cibi) > 0 else 0
	res_any = sum(distances_any)/len(distances_any) if len(distances_any) > 0 else 0
	res_both = sum(distances_both)/len(distances_both) if len(distances_both) > 0 else 0

	return res_ate, res_cibi, res_any, res_both

'''

'''
def average_distance_between_administrations(algo_data) -> Tuple[float, float, float, float, float]:
    """
	Returns a tuple of four numbers:
	the first is the average distance (in weeks) between administrations of Atezolizumab; 
	the second is the average distance (in weeks) between administrations of Cibisatamab; 
	the third is the average distance (in weeks) between administrations of any drugs; 
	the fourth is the average distance (in weeks) between combined administrations of both drugs; 
	"""
    d_adm_ate = []
    d_adm_cibi = []
    d_adm_any = []
    d_adm_both = []
    for vp_id, vp_data in algo_data.items():
        distances_ate, distances_cibi, distances_any, distances_both = distance_between_administrations(vp_data)
        d_adm_ate += distances_ate
        d_adm_cibi += distances_cibi
        d_adm_any += distances_any
        d_adm_both += distances_both

    res_ate = sum(d_adm_ate) / len(d_adm_ate)
    res_cibi = sum(d_adm_cibi) / len(d_adm_cibi)
    mean_any, std_any = mean_and_std(d_adm_any)
    res_both = sum(d_adm_both) / len(d_adm_both)

    return res_ate, res_cibi, mean_any, res_both, std_any
'''

def average_number_of_administrations(algo_data) -> Tuple[float, float, float, float, float, float, float, float]:
    """
	Returns a tuple of four numbers:
	the first is the average number of Atezolizumab administrations;
	the second is the average number of Cibisatamab administrations;
	the third is the average number of administrations of at least one drug;
	the fourth is the average number of combined administrations of both drugs;
	"""
    n_adm_ate = []
    n_adm_cibi = []
    n_adm_any = []
    n_adm_both = []
    for vp_id, vp_data in algo_data.items():

        therapy = get_therapy_by_week(vp_data)
        adm_ate = 0
        adm_cibi = 0
        adm_any = 0
        adm_both = 0

        for week, adm in enumerate(therapy):
            ate, cibi = adm
            if ate >= MIN_DOSE_ATE or cibi >= MIN_DOSE_CIBI:
                adm_any += 1
            if ate >= MIN_DOSE_ATE and cibi >= MIN_DOSE_CIBI:
                adm_both += 1
            if ate >= MIN_DOSE_ATE:
                adm_ate += 1
            if cibi >= MIN_DOSE_CIBI:
                adm_cibi += 1

        n_adm_ate += [adm_ate]
        n_adm_cibi += [adm_cibi]
        n_adm_any += [adm_any]
        n_adm_both += [adm_both]

    mean_ate, std_ate = mean_and_std(n_adm_ate)
    mean_cibi, std_cibi = mean_and_std(n_adm_cibi)
    mean_any, std_any = mean_and_std(n_adm_any)
    mean_both, std_both = mean_and_std(n_adm_both)
    return mean_ate, std_ate, mean_cibi, std_cibi, mean_any, std_any, mean_both, std_both


def average_max_time_between_administrations(algo_data: dict) -> Tuple[float, float]:
    """
	Returns the average of the maximum distance between consecutive administrations
	over all patients. The first element is the average for Ate administrations,
	the second for Cibi, the third for any administration and the fourth for
	administration of both.
	"""
    max_time = []

    for vp_id, vp_data in algo_data.items():
        max_vp = max_time_between_administrations(vp_data)
        max_time += [max_vp] if max_vp > 0 else []

    return mean_and_std(max_time)


def average_doses_used(algo_data: dict) -> Tuple[float, float, float, float]:
    """Returns the average doses (with standard errors) of Atezolizumab and Cibisatamab used by the optimal therapies
	computed by the algorithm."""
    ate_doses, cibi_doses = [], []

    for _, vp_data in algo_data.items():
        vp_ate_doses, vp_cibi_doses = doses_for_vp(vp_data)
        ate_doses += vp_ate_doses
        cibi_doses += vp_cibi_doses
        # print(f"Ate doses: {ate_doses}")
        # print(f"Mean dose: {mean_and_std(vp_ate_doses)[0]}")


    mean_ate, std_ate = mean_and_std(ate_doses)
    mean_cibi, std_cibi = mean_and_std(cibi_doses)


    return mean_ate, std_ate, mean_cibi, std_cibi


def all_algo_analyses(data: dict) -> dict:
    results = dict()
    for algo, algo_data in data.items():
        algo_results = dict()

        # 1. Average distances between administrations
        avg_distances_results = dict()
        avg_d_ate, avg_d_cibi, avg_d_any, avg_d_both, std_d_any = average_distance_between_administrations(algo_data)
        # avg_distances_results["ate"] = avg_d_ate
        # avg_distances_results["cibi"] = avg_d_cibi
        avg_distances_results["mean"] = avg_d_any
        avg_distances_results["std"] = std_d_any
        # avg_distances_results["both"] = avg_d_both
        algo_results["average_distances_between_adm"] = avg_distances_results

        # 2. Average number of administrations
        avg_number_results = dict()
        avg_n_ate, std_n_ate, avg_n_cibi, std_n_cibi, \
        avg_n_any, std_n_any, avg_n_both, std_n_both = average_number_of_administrations(algo_data)
        avg_number_results["ate"] = {'mean': avg_n_ate, 'std': std_n_ate}
        avg_number_results["cibi"] = {'mean': avg_n_cibi, 'std': std_n_cibi}
        avg_number_results["any"] = {'mean': avg_n_any, 'std': std_n_any}
        avg_number_results["both"] = {'mean': avg_n_both, 'std': std_n_both}
        algo_results["average_number_adm"] = avg_number_results

        # 3. Average used dose
        avg_doses_results = dict()
        avg_dose_ate, std_dose_ate, avg_dose_cibi, std_dose_cibi = average_doses_used(algo_data)
        avg_doses_results["ate"] = {"mean": avg_dose_ate, "std": std_dose_ate}
        avg_doses_results["cibi"] = {"mean": avg_dose_cibi, "std": std_dose_cibi}
        algo_results["mean_used_doses"] = avg_doses_results

        # 4. Average maximum length of a period without administrations
        mean_max_time, std_max_time = average_max_time_between_administrations(algo_data)
        avg_max_time_btw_adm_results = dict()
        avg_max_time_btw_adm_results["mean"] = mean_max_time
        avg_max_time_btw_adm_results["std"] = std_max_time
        algo_results["average_max_period_without_adm"] = avg_max_time_btw_adm_results

        # 5. Time to response
        avg_time_to_response = dict()
        mean_std_ttr, n_vp = mean_time_to_response(algo_data)
        mean_ttr, std_ttr = mean_std_ttr
        avg_time_to_response["mean"] = mean_ttr
        avg_time_to_response["std"] = std_ttr
        avg_time_to_response["num_vps"] = n_vp
        algo_results["time_to_response"] = avg_time_to_response

        results[algo] = algo_results

    results["ranking"] = algorithm_ranking(data)

    return results


def all_algo_analyses_by_KPI(data: dict) -> dict:
    results = {"ranking": algorithm_ranking(data),
               "time_to_response": dict(),
               "average_distances_between_adm": dict(),
               "average_number_adm": dict(),
               "mean_used_doses": dict(),
               "average_max_period_without_adm": dict()}

    for algo, algo_data in data.items():

        # 1. Average distances between administrations
        '''avg_distances_results = dict()
        avg_d_ate, avg_d_cibi, avg_d_any, avg_d_both, std_d_any = average_distance_between_administrations(algo_data)
        # avg_distances_results["ate"] = avg_d_ate
        # avg_distances_results["cibi"] = avg_d_cibi
        avg_distances_results["mean"] = avg_d_any
        avg_distances_results["std"] = std_d_any
        # avg_distances_results["both"] = avg_d_both
        results["average_distances_between_adm"][algo] = avg_distances_results'''

        # 2. Average number of administrations
        avg_number_results = dict()
        avg_n_ate, std_n_ate, avg_n_cibi, std_n_cibi, \
        avg_n_any, std_n_any, avg_n_both, std_n_both = average_number_of_administrations(algo_data)
        avg_number_results["ate"] = {'mean': avg_n_ate, 'std': std_n_ate}
        avg_number_results["cibi"] = {'mean': avg_n_cibi, 'std': std_n_cibi}
        avg_number_results["any"] = {'mean': avg_n_any, 'std': std_n_any}
        avg_number_results["both"] = {'mean': avg_n_both, 'std': std_n_both}
        results["average_number_adm"][algo] = avg_number_results

        # 3. Average used dose
        avg_doses_results = dict()
        avg_dose_ate, std_dose_ate, avg_dose_cibi, std_dose_cibi = average_doses_used(algo_data)
        avg_doses_results["ate"] = {"mean": avg_dose_ate, "std": std_dose_ate}
        avg_doses_results["cibi"] = {"mean": avg_dose_cibi, "std": std_dose_cibi}
        results["mean_used_doses"][algo] = avg_doses_results

        # 4. Average maximum length of a period without administrations
        mean_max_time, std_max_time = average_max_time_between_administrations(algo_data)
        avg_max_time_btw_adm_results = dict()
        avg_max_time_btw_adm_results["mean"] = mean_max_time
        avg_max_time_btw_adm_results["std"] = std_max_time
        results["average_max_period_without_adm"][algo] = avg_max_time_btw_adm_results

        # 5. Time to response
        avg_time_to_response = dict()
        mean_std_ttr, n_vp = mean_time_to_response(algo_data)
        mean_ttr, std_ttr = mean_std_ttr
        avg_time_to_response["mean"] = mean_ttr
        avg_time_to_response["std"] = std_ttr
        avg_time_to_response["num_vps"] = n_vp
        results["time_to_response"][algo] = avg_time_to_response

    return results


def test():
    filename = "experimental_results.json"
    f = open(filename, "r")
    data = json.load(f)

    algorithm = "hooke_jeeves"
    patient = "virtual_patient_1_p1"
    algo_data = data[algorithm]
    vp = algo_data[patient]
    print(f"doses: {doses_for_vp(vp)}")
    print(f"Results for patient {patient}:\n")
    print(f"Time to response: {time_to_response_vp(vp)}\n")
    print(f"Tumour growth: {final_over_initial(vp)}\n")
    print(f"Atezolizumab parameters:")
    print(f"Frequency of administration: {myround(vp['ate_drug'][0])}")
    for mi, dose in enumerate(vp['ate_drug'][1:]):
        print(f"Month {mi + 1}: {dose if dose >= MIN_DOSE_ATE else 0}")

    print(f"Cibisatamab parameters:")
    print(f"Frequency of administration: {myround(vp['cibi_drug'][0])}")
    for mi, dose in enumerate(vp['cibi_drug'][1:]):
        print(f"Month {mi + 1}: {dose if dose >= MIN_DOSE_CIBI else 0}")

    print("\nTherapy:")
    therapy = get_therapy_by_week(vp)


    for week, doses in enumerate(therapy):
        print(f"Week {week}: {doses[0]} Ate - {doses[1]} Cibi {'====' if week % 5 == 0 else ''}")

    '''for week, doses in enumerate(therapy):
        # if doses != (0, 0):
        print(f"Week {week + 1}: {doses[0]} Ate - {doses[1]} Cibi {'====' if week%5==0 else ''}")'''
    '''print(f"\nMax period between two administrations: {max_time_between_administrations(vp)} weeks\n")

    avg_dist_ate, avg_dist_cibi, avg_dist_any, avg_dist_both = average_distance_between_administrations(vp)
	print(f"Average distance between administration: Ate: {avg_dist_ate} - Cibi: {avg_dist_cibi} - Any: {avg_dist_any} - Both: {avg_dist_both}\n")
	

    avg_n_ate, avg_n_cibi, avg_n_any, avg_n_both = average_number_of_administrations(algo_data)
    print(
        f"Average number of administrations for algorithm '{algorithm}': Ate: {avg_n_ate} - Cibi: {avg_n_cibi} - Any: {avg_n_any} - Both: {avg_n_both}\n")

    avg_d_ate, avg_d_cibi, avg_d_any, avg_d_both, std_d_any = average_distance_between_administrations(algo_data)
    print(
        f"Average distance between consecutive administrations for algorithm '{algorithm}': Ate: {avg_d_ate} - Cibi: {avg_d_cibi} - Any: {avg_d_any} - Both: {avg_d_both}\n")

    avg_max_time = average_max_time_between_administrations(algo_data)
    print(
        f"Average length of longest period max_time_between_administrations for algorithm '{algorithm}': {avg_max_time}\n")
'''

if __name__ == "__main__":
    # test()
    # sys.exit(0)
    filename = "experimental_results.json"
    f = open(filename, "r")
    data = json.load(f)
    results = all_algo_analyses_by_KPI(data)
    out_fname = "analysis_results_by_KPI.json"
    out_file = open(out_fname, "w+")
    json.dump(results, out_file, indent=4)
    f.close()
    out_file.close()

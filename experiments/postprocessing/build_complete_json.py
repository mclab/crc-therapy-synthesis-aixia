import json

OUTPUT_PATIENT_PREFIX = "../../paper_experiments/synthesis"


def load_patient_data(algo, patient_index, pop_index):
    experiments_prefix = f"{OUTPUT_PATIENT_PREFIX}/{algo}"
    if algo == 'steepest_descent':
        experiments_prefix = f"{OUTPUT_PATIENT_PREFIX}/{algo}/low_tolerance"
    patient_prefix = f"{experiments_prefix}/patients_population_{pop_index}/virtual_patient_{patient_index}"
    simulation_json_path = f"{patient_prefix}/simulation_data_json_virtual_patient_{patient_index}_{algo}.json"
    with open(simulation_json_path, 'r') as f:
        vp_data = json.load(f)
    return vp_data



def main():
    algorithms = ['differential_evolution' , 'evolutionary_programming', 'evolutionary_strategy_sr',
     'hooke_jeeves', 'praxis', 'scatter_search', 'steepest_descent',
     'random_search', 'nelder_mead', 'genetic_algorithm', 'particle_swarm']

    population_1_size = 11
    population_2_size = 14

    all_data_json = '../../plots/data/all_patients_optimisation_data.json'
    all_data = dict()
    with open(all_data_json, 'w') as f:
        for algo in algorithms:
            all_data[algo] = dict()
            all_data[algo]['population_1'] = dict()
            for i in range(population_1_size):
                vp_data = load_patient_data(algo, patient_index=i, pop_index=1)
                all_data[algo]['population_1'][f'virtual_patient_{i}'] = vp_data

            all_data[algo]['population_2'] = dict()
            for j in range(population_2_size):
                vp_data = load_patient_data(algo, patient_index=j, pop_index=2)
                all_data[algo]['population_2'][f'virtual_patient_{j}'] = vp_data

        json.dump(all_data, f, indent=4)



if __name__ == '__main__':
    main()
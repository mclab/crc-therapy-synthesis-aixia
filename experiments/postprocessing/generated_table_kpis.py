import json



def main():
    data_json_path = "../plots/data/analysis_results_by_KPI.json"
    table_path = "table_kpi.tex"

    names_algo = ['Diff. Ev.', 'Ev. Prog.',  'Neld. M.', 'Steep. D.', 'Praxis', 'Rand. S.',
                  'SRES', 'Gen. Alg.', 'Part. Sw.', 'Scat. S.', 'H.\&J.']
    names_algo.sort()

    algorithms = ['reference', 'differential_evolution', 'evolutionary_programming', 'genetic_algorithm','hooke_jeeves',
                  'nelder_mead', 'particle_swarm', 'praxis', 'random_search', 'evolutionary_strategy_sr', 'scatter_search',
                'steepest_descent']

    with open(data_json_path, 'r') as f1:
        json_data = json.load(f1)

    #algorithms = [algo for algo in list(json_data['average_distances_between_adm'].keys()) if algo != "reference"]
    #algorithms.sort()
    names_algo = ['Ref'] + names_algo
    print(algorithms)
    print(names_algo)
    kpis = ['average_number_adm', 'mean_used_doses', 'average_max_period_without_adm']

    with open(table_path, 'w') as f:
        f.write("\\begin{tabular}{c|c|c|c|c|c}\n"
                "\\hline\n")
        header = 'Algorithm & \multicolumn{2}{c|}{\#Admin} & \multicolumn{2}{c|}{Dose} &  Max weeks w/out adm\\\\\n'
        f.write(header)
        f.write("\cline{2-3}\n")
        f.write("\cline{4-5}\n")
        f.write("& Any & Both & Ate & Cibi & \\\\\n")
        f.write("\hline\n")
        i = 0
        for algo in algorithms:
            line = f"{names_algo[i]}  &  "
            i += 1
            for kpi in kpis:
                if kpi == 'mean_used_doses':
                    if True:
                        line += f"{round(json_data[kpi][algo]['ate']['mean'], 2)} mg/kg &  " \
                                f"{round(json_data[kpi][algo]['cibi']['mean'], 2)} mg  &"
                    else:
                        line += f"{round(json_data[kpi][algo]['ate']['mean'], 2)} $\pm$ {round(json_data[kpi][algo]['ate']['std'], 2)} mg/kg &  " \
                                f"{round(json_data[kpi][algo]['cibi']['mean'], 2)} $\pm$ {round(json_data[kpi][algo]['cibi']['std'], 2)} mg"
                elif kpi == 'average_number_adm':
                    line += f"{round(json_data[kpi][algo]['any']['mean'], 2)} $\pm$ {round(json_data[kpi][algo]['any']['std'], 2)} & "
                    line += f"{round(json_data[kpi][algo]['both']['mean'], 2)} $\pm$ {round(json_data[kpi][algo]['both']['std'], 2)}  &"
                elif kpi == 'average_max_period_without_adm':
                    line += f"{round(json_data[kpi][algo]['mean'], 2)} $\pm$ {round(json_data[kpi][algo]['std'], 2)}\\\\"
                else:
                    line += f"{round(json_data[kpi][algo]['mean'], 2)} $\pm$ {round(json_data[kpi][algo]['std'], 2)} &"
            f.write(f"{line}\n")
        f.write("\hline\n")
        f.write("\end{tabular}")


if __name__ == '__main__':
    main()

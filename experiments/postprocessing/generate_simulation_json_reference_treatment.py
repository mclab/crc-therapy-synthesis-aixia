from apricopt.model.Model import Model
from simulation_lib.simulation import get_model, get_population, get_initial_treatment

import pandas as pd
import json
import glob

EXCLUDE_FROM_INIT = "../../../case_studies/CRC/therapy_synthesis/input/extras/init_exclude.json"
OBSERVED_OUTPUTS = "../../../case_studies/CRC/therapy_synthesis/input/extras/observed_outputs_post_simulation.json"
MODEL_PATH = "../../../case_studies/CRC/models/CRC_optimisation_L3V1_treatment_time_function_copasi_AIxIA_proceedings.xml"
OUTPUT_PATH = "../../plots/data/all_patients_reference_data.json"
OUTPUT_PATH_2 = "../../plots/data/all_patients_reference_data_NEW.json"
PATIENTS_POPULATION_1 = "../../../case_studies/CRC/virtual_population/11_patients_population_1.tsv"
PATIENTS_POPULATION_2 = "../../../case_studies/CRC/virtual_population/14_patients_population_2.tsv"
INITIAL_TREATMENT = "../../../case_studies/CRC/therapy_synthesis/input/tsv/initial_treatment.tsv"


def simulate_patients(model: Model, sim_engine, therapy, patients_frame, patient_index: int):
    with open(EXCLUDE_FROM_INIT) as f:
        exclude_from_initialization = json.load(f)

    patient = patients_frame.loc[patient_index]
    virtual_patient = dict()
    for k, val in patient.items():
        virtual_patient[k] = val

    init_time = virtual_patient['init_time']
    vp = {param_name: virtual_patient[param_name] for param_name in virtual_patient if
          param_name not in ['id', 'admissible', 'init_time', 'conditionId']}
    model.set_fixed_params(vp)
    try:
        sim_engine.simulate_and_set(model, init_time, exclude=exclude_from_initialization,
                                    evaluate_constraints=False)
    except:
        print(f"Instability problem with virtual patient {patient_index} initialisation...")

    model.set_fixed_params(therapy)

    try:
        trajectory = sim_engine.simulate_trajectory(model, 400)
    except:
        print(f"Treatment simulation instability of virtual patient {patient_index}...")

    model.reinit()
    return trajectory


def write_vp_json(trajectories, therapy, i, algo, patient_prefix):
    data = dict()
    data['synthesized_therapy'] = therapy
    for k, traj in trajectories.items():
        if k != 'time':
            data[k] = traj[-1]
    with open(f"{patient_prefix}/virtual_patient_{i}_{algo}.json", 'w') as f1:
        json.dump(data, f1, indent=4)


def trasform_therapy(therapy):
    therapy_list_ate = [therapy['ate_freq_in']]
    therapy_list_cibi = [therapy['cibi_freq_in']]
    for i in range(1, 16):
        therapy_list_ate.append(therapy[f'ate_m{i}_in'])
        therapy_list_cibi.append(therapy[f'cibi_m{i}_in'])
    return  therapy_list_ate, therapy_list_cibi


def compute_population(population_size, reference_therapy, model, sim_engine, patients_population_frame,
                       pop_index):
    print(f"Population {pop_index}...")
    pop_data = dict()
    for i in range(population_size):
        print(f"Virtual Patient {i}...")
        vp_data = dict()

        trajectories = simulate_patients(model, sim_engine, reference_therapy, patients_population_frame, i)
        ate_list, cibi_list = trasform_therapy(reference_therapy)
        vp_data['ate_drug'] = ate_list
        vp_data['cibi_drug'] = cibi_list
        vp_data['inefficacy'] = trajectories['inefficacy'][-1]
        vp_data['total_drugs'] = trajectories['total_drugs'][-1]
        vp_data['objective_value'] = trajectories['objective'][-1]
        vp_data['tumour_diameter'] = trajectories['tumour_diameter']

        pop_data[f'virtual_patient_{i}'] = vp_data
    return pop_data

def main():
    model, sim_engine = get_model(MODEL_PATH, OBSERVED_OUTPUTS)
    therapy_refrence = get_initial_treatment(INITIAL_TREATMENT)

    patients_population_1_frame = pd.read_csv(PATIENTS_POPULATION_1, delimiter='\t')
    population_1_size = 11
    patients_population_2_frame = pd.read_csv(PATIENTS_POPULATION_2, delimiter='\t')
    population_2_size = 14

    reference_data = {'population_1': {}, 'population_2': {}}
    pop1_data = compute_population(population_size=population_1_size, reference_therapy=therapy_refrence,
                        model=model, sim_engine=sim_engine, patients_population_frame=patients_population_1_frame,
                        pop_index=1)
    reference_data['population_1'] = pop1_data

    pop2_data = compute_population(population_size=population_2_size, reference_therapy=therapy_refrence,
                           model=model, sim_engine=sim_engine, patients_population_frame=patients_population_2_frame,
                           pop_index=2)

    reference_data['population_2'] = pop2_data

    with open(OUTPUT_PATH, 'w') as f:
        json.dump(reference_data, f, indent=4)


def main_single_simulation():
    model, sim_engine = get_model(MODEL_PATH, OBSERVED_OUTPUTS)
    therapy_refrence = get_initial_treatment(INITIAL_TREATMENT)
    patients_population_2_frame = pd.read_csv(PATIENTS_POPULATION_2, delimiter='\t')
    trajectories = simulate_patients(model, sim_engine, therapy_refrence, patients_population_2_frame, 4)

    vp_data = dict()
    ate_list, cibi_list = trasform_therapy(therapy_refrence)
    vp_data['ate_drug'] = ate_list
    vp_data['cibi_drug'] = cibi_list
    vp_data['inefficacy'] = trajectories['inefficacy'][-1]
    vp_data['total_drugs'] = trajectories['total_drugs'][-1]
    vp_data['objective_value'] = trajectories['objective'][-1]
    vp_data['tumour_diameter'] = trajectories['tumour_diameter']

    with open(OUTPUT_PATH, 'r') as f:
        loaded_data = json.load(f)

    data = dict(loaded_data)
    data['population_2']['virtual_patient_4'] = vp_data
    with open(f"{OUTPUT_PATH}", 'w') as f1:
        json.dump(data, f1, indent=4)


if __name__ == '__main__':
    main()
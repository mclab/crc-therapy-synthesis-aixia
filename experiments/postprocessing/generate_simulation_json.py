from apricopt.model.Model import Model
from simulation_lib.simulation import get_model, get_population

import pandas as pd
import json
import glob

EXCLUDE_FROM_INIT = "../../../case_studies/CRC/therapy_synthesis/input/extras/init_exclude.json"
OBSERVED_OUTPUTS = "../../../case_studies/CRC/therapy_synthesis/input/extras/observed_outputs_post_simulation.json"
MODEL_PATH = "../../../case_studies/CRC/models/CRC_optimisation_L3V1_treatment_time_function_copasi_AIxIA_proceedings.xml"
OUTPUT_PATIENT_PREFIX = "../../paper_experiments/synthesis"
PATIENTS_POPULATION_1 = "../../../case_studies/CRC/virtual_population/11_patients_population_1.tsv"
PATIENTS_POPULATION_2 = "../../../case_studies/CRC/virtual_population/14_patients_population_2.tsv"



def build_therapy_params():
    params = ['ate_freq_in', 'cibi_freq_in']
    for i in range(1, 16):
        params.append(f'ate_m{i}_in')

    for i in range(1, 16):
        params.append(f'cibi_m{i}_in')

    params.sort()
    return params


def simulate_patients(model: Model, sim_engine, therapy, patients_frame, patient_index: int):
    with open(EXCLUDE_FROM_INIT) as f:
        exclude_from_initialization = json.load(f)

    patient = patients_frame.loc[patient_index]
    virtual_patient = dict()
    for k, val in patient.items():
        virtual_patient[k] = val

    init_time = virtual_patient['init_time']
    vp = {param_name: virtual_patient[param_name] for param_name in virtual_patient if
          param_name not in ['id', 'admissible', 'init_time', 'conditionId']}
    model.set_fixed_params(vp)
    try:
        sim_engine.simulate_and_set(model, init_time, exclude=exclude_from_initialization,
                                    evaluate_constraints=False)
    except:
        print(f"Instability problem with virtual patient {patient_index} initialisation...")

    model.set_fixed_params(therapy)

    try:
        trajectory = sim_engine.simulate_trajectory(model, 400)
    except:
        print(f"Treatment simulation instability of virtual patient {patient_index}...")

    model.reinit()
    return trajectory


def parse_therapy_from_line(patient_path: str, therapy_params):
    therapy = dict()
    with open(patient_path) as f:
        lines = f.readlines()
        optimal_lines = lines[-1]
        optimal_params = optimal_lines.split("\t\t\t")[-1].split("\t")
        optimal_params = optimal_params[1:-1]
        for i in range(len(optimal_params)):
            therapy[therapy_params[i]] = float(optimal_params[i])
    return therapy


def write_vp_json(trajectories, therapy, i, algo, patient_prefix):
    data = dict()
    data['synthesized_therapy'] = therapy
    for k, traj in trajectories.items():
        if k != 'time':
            data[k] = traj[-1]
    with open(f"{patient_prefix}/virtual_patient_{i}_{algo}.json", 'w') as f1:
        json.dump(data, f1, indent=4)


def trasform_therapy(therapy):
    therapy_list_ate = [therapy['ate_freq_in']]
    therapy_list_cibi = [therapy['cibi_freq_in']]
    for i in range(1, 16):
        therapy_list_ate.append(therapy[f'ate_m{i}_in'])
        therapy_list_cibi.append(therapy[f'cibi_m{i}_in'])
    return  therapy_list_ate, therapy_list_cibi


def compute_population(algo, population_size, therapy_parameters, model, sim_engine, patients_population_frame,
                       pop_index):
    print(f"Algorithm: {algo}")
    print(f"Population {pop_index}...")
    for i in range(population_size):
        print(f"Virtual Patient {i}...")
        vp_data = dict()
        experiments_prefix = f"{OUTPUT_PATIENT_PREFIX}/{algo}"
        if algo == 'steepest_descent':
            experiments_prefix = f"{OUTPUT_PATIENT_PREFIX}/{algo}/low_tolerance"
        patient_prefix = f"{experiments_prefix}/patients_population_{pop_index}/virtual_patient_{i}"
        optimal_therapy_log = f"{patient_prefix}/log_virtual_patient_{i}_{algo}_synthesis.txt"
        therapy = parse_therapy_from_line(optimal_therapy_log, therapy_parameters)
        print(therapy)
        if len(therapy.keys()) == 0:
            builded_json_path = f"{patient_prefix}/virtual_patient_{i}_{algo}.json"
            with open(builded_json_path, 'r') as f:
                loaded_results = json.load(f)
                therapy = loaded_results['synthesized_therapy']

            trajectories = simulate_patients(model, sim_engine, therapy, patients_population_frame, i)
            objective = loaded_results['objective_value']
        else:
            trajectories = simulate_patients(model, sim_engine, therapy, patients_population_frame, i)
            objective = trajectories['objective'][-1]
            print(objective)

        ate_list, cibi_list = trasform_therapy(therapy)
        vp_data['ate_drug'] = ate_list
        vp_data['cibi_drug'] = cibi_list
        vp_data['inefficacy'] = trajectories['inefficacy'][-1]
        vp_data['total_drugs'] = trajectories['total_drugs'][-1]
        vp_data['objective_value'] = objective
        vp_data['tumour_diameter'] = trajectories['tumour_diameter']

        with open(f"{patient_prefix}/simulation_data_json_virtual_patient_{i}_{algo}.json", 'w') as f:
            json.dump(vp_data, f, indent=4)


def main():
    model, sim_engine = get_model(MODEL_PATH, OBSERVED_OUTPUTS)
    therapy_parameters = build_therapy_params()

    patients_population_1_frame = pd.read_csv(PATIENTS_POPULATION_1, delimiter='\t')
    population_1_size = 11
    patients_population_2_frame = pd.read_csv(PATIENTS_POPULATION_2, delimiter='\t')
    population_2_size = 14

    algorithms = ['differential_evolution']# , 'evolutionary_programming', 'evolutionary_strategy_sr',
                 #'hooke_jeeves', 'praxis', 'scatter_search', 'steepest_descent',
                 # 'random_search', 'nelder_mead', 'genetic_algorithm', 'particle_swarm']

    all_data_json = '../../plots/data/all_patients_optimisation_data_tmp.json'
    #for algo in algorithms:
    algo = 'particle_swarm'
    compute_population(algo=algo, population_size=population_1_size, therapy_parameters=therapy_parameters,
                        model=model, sim_engine=sim_engine, patients_population_frame=patients_population_1_frame,
                        pop_index=1)

    compute_population(algo=algo, population_size=population_2_size, therapy_parameters=therapy_parameters,
                           model=model, sim_engine=sim_engine, patients_population_frame=patients_population_2_frame,
                           pop_index=2)

if __name__ == '__main__':
    main()
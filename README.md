# A Comparative Study of Simulation-based AI Search Methods for Personalised Cancer Therapy Synthesis in COPASI #

This repository contains the full source code to reproduce all experiments shown in the paper and execute additional ones.

It also contains all the figures of the paper, as well as the code to re-generate them.

### COPASI algorithms
The details of the implementations of COPASI's parameter optimisation algorithms is avaliable [here](http://copasi.org/Support/User_Manual/Methods/Optimization_Methods).


## How to run

### Requirements

Our tool depends on the following requirements:

* [Python >= 3.8](https://www.python.org/)

### Installation

To install the tool execute the following instructions:
```
cd crc-therapy-synthesis-aixia
virtualenv --system-site-packages .venv
source .venv/bin/activate
pip install -r requirements.txt
```

## Run

To run the optimisation execute the following command:

```
python run.py --config=case_studies/CRC/therapy_synthesis/input/yaml/therapy_synthesis.yaml -p 0 -l /absolute/path/to/case_studies/CRC/therapy_synthesis/output/log_optimization_example.txt -o case_studies/CRC/therapy_synthesis/output/virtual_patient.json
```

* `--config`is a mandatory input argument to specify the YAML configuration file.
* `-p` is an optional input argument to specify the index of the Virtual Patient for which the therapy will be optimised. If any value is not provided, the tool will use the VP index inside the YAML file.
* `-l` is an optional input argument to specify the optimisation log file. Otherwise, the tool will use the output path inside the YAML file. (Warning: due to a bug of COPASI, you can only specify an absolute path or a relative path of this type "optimisation_log.txt"). In the latter case, you will find the optimisation log within the model folder.
* `-o` is an optional input argument to set the path for the output therapy. Otherwise, the tool will use the path inside the YAML file.


### Experiments

The directory `case_studies/CRC/therapy_synthesis/yaml/experiments/` contains the YAML configuration files used to perform all experiments. 
To replicate an experiment you must select a YAML file and adequately set the above options (or the YAML file).


#### Experiments data
All experiments are sited in `experiments/paper_experiments`. 

Postprocessed data are sited in `experiments/plots/data`.



### Plots

To generate all plots run the related scripts as follows:

```
cd experiments/plots
./generate_plots.sh
```

You will find all plots inside the folder: `experiments/plots/figures/`


## Authors

* [Marco Esposito](esposito@di.uniroma1.it)
* [Leonardo Picchiami](picchiami@di.uniroma1.it)

